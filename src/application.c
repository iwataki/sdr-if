/*
 * application.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */
#include "application.h"
#include "task.h"
#include "hardware.h"
#include "system_state_store.h"
#include "vfo_output_task.h"
#include "rssi_observe_task.h"
#include "controlpin_observe_task.h"
#include "ptt_observe_normal_task.h"
#include "radio_circuit_control_task.h"
#include "process_commands.h"
#include "modulation_mode_control_task.h"
#include "serial_command_transport.h"
#include "command_port_mapping.h"
#include "commands.h"
#include "sqeulch_control_task.h"

TASK_LIST_t application_task_list;
APPLICATION_TASK_ID_TABLE_t task_id_table;

void init_application(void)
{
	init_system_status();
	init_task(&application_task_list);
	vfo_output_task_init(&system_status);
	task_id_table.vfo_output_task_id = create_task(&application_task_list, vfo_output_task, 0);
	rssi_observe_task_init(&system_status);
	task_id_table.rssi_observe_task_id = create_task(&application_task_list, rssi_observe_task, 0);
	controlpin_observe_task_init(&system_status);
	task_id_table.controlpin_input_observe_task_id = create_task(&application_task_list, controlpin_observe_task, 0);
	ptt_observe_normal_task_init(&system_status, &ptt_key);
	task_id_table.ptt_observe_normal_task_id = create_task(&application_task_list, ptt_observe_normal_task, ptt_observe_normal_task_on_activated);
	radio_circuit_control_task_init(&system_status);
	task_id_table.radio_circuit_ctrl_task_id = create_task(&application_task_list, radio_circuit_control_task, 0);
	command_processor_init(serial_command_transport_init(get_serial_funcs(1)));
	commands_init(&system_status);
	task_id_table.command_process_task_id = create_task(&application_task_list, command_processor_poll, 0);
	modulation_mode_control_task_init(&system_status);
	task_id_table.modulation_mode_observe_task_id = create_task(&application_task_list, modulation_mode_control_task, 0);
	sql_ctrl_task_init(&system_status);
	task_id_table.sql_ctrl_task_id = create_task(&application_task_list, sql_ctrl_task, 0);
}

void process_application(void)
{
	do_task(&application_task_list);
}
