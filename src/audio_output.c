#include "audio_output.h"
#include "stm32f30x_conf.h"
#include <stddef.h>

static write_callback_t m_write_callback;
static size_t m_buffer_size;
static uint16_t *mp_buffer;
typedef struct
{
    uint32_t rcc_gpio;
    GPIO_TypeDef *gpio;
    uint32_t rcc_dac;
    DAC_TypeDef *dac;
    DMA_Channel_TypeDef *dma;
    uint16_t pin;
    uint8_t dac_pin_no;
    uint8_t irq_num;
    uint8_t chan;

} dac_resource_map_item_t;
static const dac_resource_map_item_t dac_resource_map[] = {
    {RCC_AHBPeriph_GPIOA, GPIOA, RCC_APB1Periph_DAC1, DAC1, DMA1_Channel3, GPIO_Pin_4, 1, DMA1_Channel3_IRQn, 1},
    {RCC_AHBPeriph_GPIOA, GPIOA, RCC_APB1Periph_DAC1, DAC1, DMA1_Channel4, GPIO_Pin_5, 2, DMA1_Channel4_IRQn, 2},
    {RCC_AHBPeriph_GPIOA, GPIOA, RCC_APB1Periph_DAC2, DAC2, DMA1_Channel5, GPIO_Pin_6, 3, DMA1_Channel5_IRQn, 1},
    {0, 0, 0, 0, 0, 0}};

static void init_dac(const dac_resource_map_item_t *dac_resource)
{
    RCC_APB1PeriphClockCmd(dac_resource->rcc_dac, ENABLE);
    DAC_InitTypeDef dac_initdata;
    DAC_StructInit(&dac_initdata);
    dac_initdata.DAC_Trigger = DAC_Trigger_T6_TRGO;
    dac_initdata.DAC_Buffer_Switch = DAC_BufferSwitch_Enable;
    DAC_Init(dac_resource->dac, (dac_resource->chan == 1) ? DAC_Channel_1 : DAC_Channel_2, &dac_initdata);
}

static void init_timer(uint32_t samplingrate)
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
    TIM_TimeBaseInitTypeDef timebaseinit;
    TIM_TimeBaseStructInit(&timebaseinit);
    timebaseinit.TIM_CounterMode = TIM_CounterMode_Up;
    timebaseinit.TIM_ClockDivision = TIM_CKD_DIV1;
    timebaseinit.TIM_RepetitionCounter = 0;
    timebaseinit.TIM_Prescaler = 0;
    timebaseinit.TIM_Period = SystemCoreClock / samplingrate;
    TIM_TimeBaseInit(TIM6, &timebaseinit);
    TIM_SelectOutputTrigger(TIM6, TIM_TRGOSource_Update);
}

static void init_dma(const dac_resource_map_item_t *dac_resource, uint16_t *buffer, size_t buflen, uint8_t ch)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG,ENABLE);
    SYSCFG_DMAChannelRemapConfig(SYSCFG_DMARemap_DAC2Ch1, ENABLE);
    SYSCFG_DMAChannelRemapConfig(SYSCFG_DMARemap_TIM6DAC1Ch1, ENABLE);
    SYSCFG_DMAChannelRemapConfig(SYSCFG_DMARemap_TIM7DAC1Ch2, ENABLE);
    NVIC_InitTypeDef nvic_init;
    nvic_init.NVIC_IRQChannelCmd = ENABLE;
    nvic_init.NVIC_IRQChannel = dac_resource->irq_num;
    nvic_init.NVIC_IRQChannelPreemptionPriority = 0;
    nvic_init.NVIC_IRQChannelSubPriority = 0;
    NVIC_Init(&nvic_init);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
    DMA_InitTypeDef dmainit;
    DMA_StructInit(&dmainit);
    dmainit.DMA_PeripheralBaseAddr = (uint32_t)((ch == 1) ? &(dac_resource->dac->DHR12R1) : &(dac_resource->dac->DHR12R2));
    dmainit.DMA_MemoryBaseAddr = (uint32_t)buffer;
    dmainit.DMA_DIR = DMA_DIR_PeripheralDST; //buf -> adc data regster 
    dmainit.DMA_BufferSize = buflen;
    dmainit.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    dmainit.DMA_MemoryInc = DMA_MemoryInc_Enable;
    dmainit.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    dmainit.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    dmainit.DMA_Mode = DMA_Mode_Circular;
    dmainit.DMA_Priority = DMA_Priority_High;
    DMA_Init(dac_resource->dma, &dmainit);
    DMA_ITConfig(dac_resource->dma, DMA_IT_HT | DMA_IT_TC, ENABLE);
}

static const dac_resource_map_item_t *get_dac_resource_map(uint8_t chan)
{
    for (int i = 0; dac_resource_map[i].dac_pin_no != 0; i++)
    {
        if (chan == dac_resource_map[i].dac_pin_no)
        {
            return &dac_resource_map[i];
        }
    }
    return 0;
}

static int8_t set_gpio_as_analogout(uint8_t chan)
{
    const dac_resource_map_item_t *gpio_map = get_dac_resource_map(chan);
    if (gpio_map == 0)
    {
        return -1;
    }
    RCC_AHBPeriphClockCmd(gpio_map->rcc_gpio, ENABLE);
    GPIO_InitTypeDef gpio_init_data;
    GPIO_StructInit(&gpio_init_data);
    gpio_init_data.GPIO_Mode = GPIO_Mode_AN;
    gpio_init_data.GPIO_PuPd = GPIO_PuPd_NOPULL;
    gpio_init_data.GPIO_Speed = GPIO_Speed_2MHz;
    gpio_init_data.GPIO_Pin = gpio_map->pin;
    GPIO_Init(gpio_map->gpio, &gpio_init_data);
    return 0;
}
static int8_t gpio_free(uint8_t chan)
{
    const dac_resource_map_item_t *gpio_map = get_dac_resource_map(chan);
    if (gpio_map == 0)
    {
        return -1;
    }
    GPIO_InitTypeDef gpio_init_data;
    GPIO_StructInit(&gpio_init_data);
    gpio_init_data.GPIO_Pin = gpio_map->pin;
    GPIO_Init(gpio_map->gpio, &gpio_init_data);
    return 0;
}

void audio_output_init(uint8_t chan, uint32_t samplingrate, uint16_t *buffer, uint16_t size)
{
    const dac_resource_map_item_t *resource = get_dac_resource_map(chan);
    if (chan == 0)
    {
        return;
    }
    init_timer(samplingrate);
    init_dma(resource, buffer, size, resource->chan);
    mp_buffer = buffer;
    m_buffer_size = size;
    m_write_callback = 0;
}
int8_t audio_output_start(uint8_t chan, write_callback_t cb)
{

    m_write_callback = cb;
    int8_t retcode = set_gpio_as_analogout(chan);
    if (retcode < 0)
    {
        return retcode;
    }
    const dac_resource_map_item_t *resource = get_dac_resource_map(chan);
    if (resource == 0)
    {
        return -1;
    }
    DMA_Cmd(resource->dma, ENABLE);
    init_dac(resource);
    DAC_DMACmd(resource->dac, (resource->chan == 1) ? DAC_Channel_1 : DAC_Channel_2, ENABLE);
    DAC_Cmd(resource->dac, (resource->chan == 1) ? DAC_Channel_1 : DAC_Channel_2, ENABLE);
    TIM_Cmd(TIM6, ENABLE);
    return 0;
}
int8_t audio_output_stop(uint8_t chan)
{
    const dac_resource_map_item_t *resource = get_dac_resource_map(chan);
    if (resource == 0)
    {
        return -1;
    }
    TIM_Cmd(TIM6, DISABLE);
    DAC_DMACmd(resource->dac, (resource->chan == 1) ? DAC_Channel_1 : DAC_Channel_2, DISABLE);
    DMA_Cmd(resource->dma, DISABLE);
    DAC_DeInit(resource->dac);
    return gpio_free(chan);
}
void audio_output_get_buffer(uint8_t chan, uint16_t **pp_buffer, uint16_t *p_size)
{
    const dac_resource_map_item_t *resource = get_dac_resource_map(chan);
    if (resource == 0)
    {
        *pp_buffer = 0;
        *p_size = 0;
        return;
    }
    uint16_t remain = DMA_GetCurrDataCounter(resource->dma);
    if (remain <= m_buffer_size / 2)
    { //DMA is reading latter half;
        *pp_buffer = mp_buffer;
    }
    else
    {
        *pp_buffer = &mp_buffer[m_buffer_size / 2];
    }
    *p_size = m_buffer_size / 2;
}

void audio_output_enable(uint8_t ch)
{
    const dac_resource_map_item_t *resource = get_dac_resource_map(ch);
    if (resource->chan == 1)
    {
        resource->dac->CR |= DAC_BufferSwitch_Enable;
    }
    else
    {
        resource->dac->CR |= DAC_BufferSwitch_Enable << 16;
    }
}
void audio_output_disable(uint8_t ch)
{
    const dac_resource_map_item_t *resource = get_dac_resource_map(ch);
    if (resource->chan == 1)
    {
        resource->dac->CR &= ~DAC_BufferSwitch_Enable;
    }
    else
    {
        resource->dac->CR &= ~(DAC_BufferSwitch_Enable << 16);
    }
}

void DMA1_Channel3_IRQHandler()
{
    if(DMA_GetITStatus(DMA1_IT_HT3)){
        DMA_ClearITPendingBit(DMA1_IT_HT3);
        if(m_write_callback){
            m_write_callback();
        }
    }
    if(DMA_GetITStatus(DMA1_IT_TC3)){
        DMA_ClearITPendingBit(DMA1_IT_TC3);
        if(m_write_callback){
            m_write_callback();
        }
    }

}
void DMA1_Channel4_IRQHandler()
{
       if(DMA_GetITStatus(DMA1_IT_HT4)){
        DMA_ClearITPendingBit(DMA1_IT_HT4);
        if(m_write_callback){
            m_write_callback();
        }
    }
    if(DMA_GetITStatus(DMA1_IT_TC4)){
        DMA_ClearITPendingBit(DMA1_IT_TC4);
        if(m_write_callback){
            m_write_callback();
        }
    }
}
void DMA1_Channel5_IRQHandler()
{
       if(DMA_GetITStatus(DMA1_IT_HT5)){
        DMA_ClearITPendingBit(DMA1_IT_HT5);
        if(m_write_callback){
            m_write_callback();
        }
    }
    if(DMA_GetITStatus(DMA1_IT_TC5)){
        DMA_ClearITPendingBit(DMA1_IT_TC5);
        if(m_write_callback){
            m_write_callback();
        }
    }
}