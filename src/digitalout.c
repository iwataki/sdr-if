/*
 * digitalout.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */


#include "digitalout.h"
#include "gpio.h"

void init_digitalout(DIGITALOUT_t*d){
	gpio_enable(d->gpio);
	GPIO_InitTypeDef init;
	GPIO_StructInit(&init);
	init.GPIO_Mode=GPIO_Mode_OUT;
	init.GPIO_OType=GPIO_OType_PP;
	init.GPIO_Pin=d->pin;
	init.GPIO_Speed=GPIO_Speed_2MHz;
	GPIO_Init(d->gpio,&init);
}
void digitalout_set(DIGITALOUT_t*d){
	GPIO_WriteBit(d->gpio,d->pin,Bit_SET);
}
void digitalout_reset(DIGITALOUT_t*d){
	GPIO_WriteBit(d->gpio,d->pin,Bit_RESET);
}
