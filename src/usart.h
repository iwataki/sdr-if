/*
 * uart.h
 *
 *  Created on: 2012/09/28
 *      Author: ���@�@��Y
 */

#ifndef UART_H_
#define UART_H_
#include "stdint.h"

#define BUFF_SIZE 128//2^xsize



void init_usart(int ch,uint32_t uart_brr);
void usart_putc(uint8_t ch,char c);
char usart_getc_noblock(uint8_t ch);
int usart_is_received(uint8_t ch);
char usart_getc(uint8_t ch);



#endif /* UART_H_ */
