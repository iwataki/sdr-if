/*
 * data_structure.h
 *
 *  Created on: 2020/03/16
 *      Author: 宗一郎
 */

#ifndef DATA_STRUCTURE_H_
#define DATA_STRUCTURE_H_
#include <stdint.h>


//IF周波数，クロック源の周波数キャリブレーションデータを表す構造体
typedef struct{
	uint32_t bfo_freq;//Hz
	uint32_t crystal;//Hz
}CALIBRATION_DATA_t;
uint8_t calibration_data_compare(CALIBRATION_DATA_t*d1,CALIBRATION_DATA_t*d2);


//送受信回路の動作状態
typedef struct{
	uint8_t is_transmitter_enable;
	uint8_t is_receiver_enable;

}RADIO_CIRCUIT_POWER_STATE_t;
uint8_t radio_circuit_power_state_compare(RADIO_CIRCUIT_POWER_STATE_t*d1,RADIO_CIRCUIT_POWER_STATE_t*d2);

typedef enum{
	MOD_SSB_USB,
	MOD_SSB_LSB,
	MOD_AM,
	MOD_FM,
	MOD_CW,
	MOD_INVALID
}MODULATION_MODE_t;

typedef struct{
	uint32_t freqency;//Hz
	uint8_t enable;

}AUX_VFO_OUTPUT_STATE_t;
uint8_t aux_vfo_output_state_compare(AUX_VFO_OUTPUT_STATE_t*s1,AUX_VFO_OUTPUT_STATE_t*s2);


//動作中のシステムの状態
typedef struct{
	CALIBRATION_DATA_t calibration;
	uint16_t rssi;
	uint16_t squelch_level;
	uint8_t squelch_enable;
	uint8_t operation_default_mode;
	MODULATION_MODE_t modulation_mode;
	RADIO_CIRCUIT_POWER_STATE_t radio_circuit_status;
	AUX_VFO_OUTPUT_STATE_t aux_vfo_status;
}SYSTEM_STATUS_t;
uint8_t system_status_compare(SYSTEM_STATUS_t*s1,SYSTEM_STATUS_t*s2);

#endif /* DATA_STRUCTURE_H_ */
