#pragma once
#include <stdint.h>

uint8_t uint8_decode(uint8_t*data);
int8_t int8_decode(uint8_t*data);

uint16_t uint16_decode(uint8_t*data);
int16_t int16_decode(uint8_t*data);

uint32_t uint32_decode(uint8_t*data);
int32_t int32_decode(uint8_t*data);

void uint8_encode(uint8_t*data,uint8_t src);
void int8_encode(uint8_t*data,int8_t src);

void uint16_encode(uint8_t*data,uint16_t src);
void int16_encode(uint8_t*data,int16_t src);

void uint32_encode(uint8_t*data,uint32_t src);
void int32_encode(uint8_t*data,int32_t src);