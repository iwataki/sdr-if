/*
 * user_switch.h
 *
 *  Created on: 2016/11/01
 *      Author: �@��Y
 */

#ifndef USER_SWITCH_H_
#define USER_SWITCH_H_
#include <inttypes.h>
#include "stm32f30x_conf.h"

#define USERSW_RELEASE 0
#define USERSW_HOLD 1
#define USERSW_PUSHED 2
#define USERSW_RELEASED 3
#define USERSW_NO_EVENT 4

typedef struct
{
	GPIO_TypeDef *gpio;
	uint16_t pin;
	volatile uint8_t latest_state;
	volatile uint8_t previous_state;
	volatile uint8_t latest_event;
} USERSW_t;

void init_usersw(USERSW_t *sw);
void update_switch(USERSW_t *sw);
int get_switch(USERSW_t *sw);
int get_switch_event(USERSW_t *sw);

#endif /* USER_SWITCH_H_ */
