/*
 * uart.c
 *
 *  Created on: 2012/09/28
 *      Author: 岩滝　宗一郎
 */
#include <stdlib.h>
#include "stm32f30x_conf.h"
#include "usart.h"

#define USART_SENDING 1
#define USART_NOT_SENDING 0

typedef struct ringbuff
{
	int write_p, read_p, state;
	char data[BUFF_SIZE];
} ringbuff_t; //ソフトウエアリングバッファ

typedef struct
{
	USART_TypeDef *usart;
	GPIO_TypeDef *gpio;
	ringbuff_t *tx_buf;
	ringbuff_t *rx_buf;
	uint32_t tx_gpio_pin;
	uint32_t rx_gpio_pin;
	uint32_t gpio_rcc;
	uint32_t usart_rcc;
	uint8_t tx_gpio_pinsource;
	uint8_t rx_gpio_pinsource;
	uint8_t nvic_irq_ch;
	uint8_t ch_no;

} usart_resourcemap_t;

static usart_resourcemap_t usart_resources[] = {
	{USART1, GPIOA, 0, 0, GPIO_Pin_9, GPIO_Pin_10, RCC_AHBPeriph_GPIOA, RCC_APB2Periph_USART1, GPIO_PinSource9, GPIO_PinSource10, USART1_IRQn, 1},
	{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

static usart_resourcemap_t *get_usart_resource(uint8_t ch)
{
	for (int i = 0; usart_resources[i].ch_no != 0; i++)
	{
		if (usart_resources[i].ch_no == ch)
		{
			return &usart_resources[i];
		}
	}
	return 0;
}

//以下，Cのプログラム
/*
 * USARTようにGPIOを初期化する
 *
 */
void gpio_init_for_uart(GPIO_TypeDef *GPIOx, uint16_t txpin, uint16_t rxpin); //initialize gpio for usart
void gpio_init_for_uart(GPIO_TypeDef *GPIOx, uint16_t txpin, uint16_t rxpin)
{
	GPIO_InitTypeDef gpioinitstr;
	gpioinitstr.GPIO_Mode = GPIO_Mode_AF; //TX Pin
	gpioinitstr.GPIO_Pin = txpin;
	gpioinitstr.GPIO_Speed = GPIO_Speed_50MHz;
	gpioinitstr.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(GPIOx, &gpioinitstr);
	gpioinitstr.GPIO_Mode = GPIO_Mode_AF; //RX Pin
	gpioinitstr.GPIO_PuPd = GPIO_PuPd_UP;
	gpioinitstr.GPIO_Pin = rxpin;
	GPIO_Init(GPIOx, &gpioinitstr);
}
/*
 * USART本体を初期化
 */
void usartmodule_init(USART_TypeDef *USARTx, uint32_t brr); //initialize usart
void usartmodule_init(USART_TypeDef *USARTx, uint32_t brr)
{
	USART_InitTypeDef usartinitstr;
	usartinitstr.USART_BaudRate = brr; //通信パラメータ設定
	usartinitstr.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	usartinitstr.USART_Parity = USART_Parity_No;
	usartinitstr.USART_StopBits = USART_StopBits_1;
	usartinitstr.USART_WordLength = USART_WordLength_8b;
	usartinitstr.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	USART_Init(USARTx, &usartinitstr);
}

/*
 * 割り込みの設定
 */
void nvic_init_for_uart(uint8_t NVIC_IRQChannel); //nvic initialize
void nvic_init_for_uart(uint8_t NVIC_IRQChannel)
{
	NVIC_InitTypeDef nvicinitstr;
	nvicinitstr.NVIC_IRQChannel = NVIC_IRQChannel;
	nvicinitstr.NVIC_IRQChannelCmd = ENABLE;
	nvicinitstr.NVIC_IRQChannelPreemptionPriority = 3;
	nvicinitstr.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&nvicinitstr);
}

/*
 * USART初期化
 * ch:1,2,3
 * brr:baudrate
 */
void init_usart(int ch, uint32_t brr)
{
	usart_resourcemap_t *resource = get_usart_resource(ch);
	if (resource == 0)
	{
		return;
	}
	//GPIO設定
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA|RCC_APB2Periph_AFIO,ENABLE);//GPIO clock設定
	RCC_AHBPeriphClockCmd(resource->gpio_rcc, ENABLE);
	gpio_init_for_uart(resource->gpio, resource->tx_gpio_pin, resource->rx_gpio_pin); //GPIO設定
	GPIO_PinAFConfig(resource->gpio, resource->tx_gpio_pinsource, GPIO_AF_7);
	GPIO_PinAFConfig(resource->gpio, resource->rx_gpio_pinsource, GPIO_AF_7);
	//USART設定
	RCC_APB2PeriphClockCmd(resource->usart_rcc, ENABLE); //usartモジュールにクロック供給

	resource->rx_buf = (ringbuff_t *)malloc(sizeof(ringbuff_t));
	resource->tx_buf = (ringbuff_t *)malloc(sizeof(ringbuff_t));
	resource->rx_buf->read_p = 0;
	resource->rx_buf->write_p = 0;
	resource->tx_buf->state = USART_NOT_SENDING;
	resource->tx_buf->read_p = 0;
	resource->tx_buf->write_p = 0;

	usartmodule_init(resource->usart, brr); //usart設定
	USART_ITConfig(resource->usart, USART_IT_RXNE, ENABLE);
	//NVIC設定
	nvic_init_for_uart(resource->nvic_irq_ch); //ENABLE irqn
	USART_Cmd(resource->usart, ENABLE); //usart有効
}
/*
 * 1文字送信
 */
void usart_putc(uint8_t ch, char c)
{
	usart_resourcemap_t *resource = get_usart_resource(ch);
	if (resource == 0)
	{
		return;
	}
	__disable_irq();
	if (resource->tx_buf->state == USART_NOT_SENDING)
	{
		//atomic
		USART_SendData(resource->usart, (uint16_t)c);
		USART_ITConfig(resource->usart, USART_IT_TXE, ENABLE); //送信完了割り込み許可
		resource->tx_buf->state = USART_SENDING;			   //送信中にする
	}
	else
	{
		resource->tx_buf->data[resource->tx_buf->write_p++] = c;
		resource->tx_buf->write_p &= BUFF_SIZE - 1;
	}
	__enable_irq();
}
/*
 * １文字受信
 */
char usart_getc_noblock(uint8_t ch)
{
	char c = 0;
	usart_resourcemap_t *resource = get_usart_resource(ch);
	if (resource == 0)
	{
		return 0;
	}
	if (resource->rx_buf->read_p != resource->rx_buf->write_p)
	{
		__disable_irq();
		c = resource->rx_buf->data[resource->rx_buf->read_p++];
		resource->rx_buf->read_p &= BUFF_SIZE - 1;
		__enable_irq();
	}
	return c; //データがない場合は0を返す.
}
/*
 * 受信確認
 * 受信＝１
 */
int usart_is_received(uint8_t ch)
{
	usart_resourcemap_t *resource = get_usart_resource(ch);
	if (resource == 0)
	{
		return 0;
	}
	if (resource->rx_buf->read_p != resource->rx_buf->write_p)
	{

		return 1;
	}
	else
	{
		return 0;
	}
}
/*
 * １文字受信，受信してない場合は待つ
 */
char usart1_getc(uint8_t ch)
{
	char c;
	usart_resourcemap_t *resource = get_usart_resource(ch);
	if (resource == 0)
	{
		return 0;
	}
	while (resource->rx_buf->read_p == resource->rx_buf->write_p)
		;

	__disable_irq();
	c = (char)(resource->rx_buf->data[resource->rx_buf->read_p++] & 0xff);
	resource->rx_buf->read_p &= BUFF_SIZE - 1;
	__enable_irq();
	return c;
}

static void usart_irq(usart_resourcemap_t *resource)
{
	char c;
	if (USART_GetITStatus(resource->usart, USART_IT_TXE))
	{
		if (resource->tx_buf->read_p == resource->tx_buf->write_p)
		{
			USART_ITConfig(resource->usart, USART_IT_TXE, DISABLE); //バッファに送るデータがなくなった
			resource->tx_buf->state = USART_NOT_SENDING;
		}
		else
		{
			c = resource->tx_buf->data[resource->tx_buf->read_p++];
			USART_SendData(resource->usart, (uint16_t)c);
			resource->tx_buf->read_p &= BUFF_SIZE - 1;
		}
	}
	if (USART_GetITStatus(resource->usart, USART_IT_RXNE))
	{
		c = (char)USART_ReceiveData(resource->usart);
		resource->rx_buf->data[resource->rx_buf->write_p++] = c;
		resource->rx_buf->write_p &= BUFF_SIZE - 1;
	}
}
/*
 * USART割り込み
 */
void USART1_IRQHandler(void)
{ //USART1割り込み処理
	usart_resourcemap_t *resource = get_usart_resource(1);
	usart_irq(resource);
}

void USART2_IRQHandler(void)
{ //USART2割り込み処理
	usart_resourcemap_t *resource = get_usart_resource(2);
	usart_irq(resource);
}
void USART3_IRQHandler(void)
{ //USART6割り込み処理
	usart_resourcemap_t *resource = get_usart_resource(3);
	usart_irq(resource);
}
