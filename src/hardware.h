/*
 * hardware.h
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_
#include "user_switch.h"
#include "adc.h"
#include "digitalout.h"


extern USERSW_t modesel_0;
extern USERSW_t modesel_1;
extern USERSW_t ptt_key;
extern USERSW_t key;
extern ADC_t sql_level_in;
extern USERSW_t tx_enable_in;
extern USERSW_t rx_enable_in;
extern DIGITALOUT_t transmitter_enable_out;
extern DIGITALOUT_t receiver_enable_out;

void init_hardware(void);

void update_hardware(void);//called from timer routine

#endif /* HARDWARE_H_ */
