#include "power_management.h"
#include "stm32f30x_conf.h"

void system_idle(void){
    CoreDebug->DHCSR|=0x07;
    PWR_EnterSleepMode(PWR_SLEEPEntry_WFI);
}