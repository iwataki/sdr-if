/*
 * hardware.c
 *
 *  Created on: 2020/03/17
 *      Author: �@��Y
 */

#include "hardware.h"
#include "si5351.h"
#include "usart.h"
#include "dac.h"

USERSW_t modesel_0 = {.gpio = GPIOA, .pin = GPIO_Pin_0};
USERSW_t modesel_1 = {.gpio = GPIOA, .pin = GPIO_Pin_1};
USERSW_t ptt_key = {.gpio = GPIOA, .pin = GPIO_Pin_2};
USERSW_t key = {.gpio = GPIOA, .pin = GPIO_Pin_3};
ADC_t sql_level_in;
USERSW_t tx_enable_in = {.gpio = GPIOA, .pin = GPIO_Pin_15};
USERSW_t rx_enable_in = {.gpio = GPIOA, .pin = GPIO_Pin_12};
DIGITALOUT_t transmitter_enable_out = {.gpio = GPIOA, .pin = GPIO_Pin_8};
DIGITALOUT_t receiver_enable_out = {.gpio = GPIOA, .pin = GPIO_Pin_11};

void init_hardware(void)
{
	init_si5351();
	init_usart(1, 19200);
	init_usersw(&modesel_0);
	init_usersw(&modesel_1);
	init_usersw(&ptt_key);
	init_usersw(&key);
	init_usersw(&tx_enable_in);
	init_usersw(&rx_enable_in);
	adc_init(&sql_level_in, ADC2, GPIOA, GPIO_Pin_4, ADC_Channel_4);
	init_digitalout(&transmitter_enable_out);
	init_digitalout(&receiver_enable_out);
	dac_init();
}

void update_hardware(void)
{
	update_switch(&modesel_1);
	update_switch(&modesel_0);
	update_switch(&key);
	update_switch(&ptt_key);
	update_switch(&tx_enable_in);
	update_switch(&rx_enable_in);
}
