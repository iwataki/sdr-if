/*
 * system_state_store.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#include "system_state_store.h"
#include "system_constants.h"

SYSTEM_STATUS_t system_status;

//set system state from config
void init_system_status()
{

	system_status.modulation_mode=MOD_SSB_USB;
	system_status.calibration.bfo_freq = DEFAULT_BFO_FREQ;
	system_status.calibration.crystal = DEFAULT_XTAL_FREQ;
	system_status.radio_circuit_status.is_receiver_enable = 0;
	system_status.radio_circuit_status.is_transmitter_enable = 0;
	system_status.squelch_level=0;
	system_status.rssi = 0;
	system_status.aux_vfo_status.freqency=0;
	system_status.aux_vfo_status.enable=0;
}
