#include "serial_command_transport.h"
#include <errno.h>

static serial_funcs_t m_serial_func;
static TRANSPORT_LAYER_STATE_t m_state = IDLE;
static uint8_t m_pkt_header_receiving = 0;
static uint8_t m_pkt_header = 0;
static uint8_t m_rx_pkt_buf[2 + sizeof(uint32_t)];
static int m_rx_index;

static uint8_t get_rx_pkt(uint8_t **data, size_t *size)
{
    if(m_state!=PKT_AVAILABLE){
        return EAGAIN;
    }
    m_state = IDLE;
    *data=m_rx_pkt_buf;
    *size=m_pkt_header;
    return 0;
}
static uint8_t send_tx_pkt(uint8_t *data, size_t size)
{
    m_serial_func.put_char(size&0xff);
    m_serial_func.put_char((size>>8)&0xff);
    for(int i=0;i<size;i++){
        m_serial_func.put_char(data[i]);
    }

    return 0;
}
static TRANSPORT_LAYER_STATE_t poll(void)
{
    while (m_serial_func.is_rx_char_available())
    {
        if (m_state == IDLE)
        {
            m_state = PKT_RECEVING;
            m_pkt_header_receiving = 1;
            m_pkt_header = m_serial_func.get_char();
            if (!m_serial_func.is_rx_char_available())
            {
                return m_state;
            }
        }
        if (m_state == PKT_RECEVING)
        {
            if (m_pkt_header_receiving)
            {
                m_pkt_header |= m_serial_func.get_char() << 8;
                m_pkt_header_receiving = 0;
                m_rx_index=0;
            }else{
                if(m_rx_index<m_pkt_header){
                    m_rx_pkt_buf[m_rx_index++]=m_serial_func.get_char();
                }else{
                    m_state=PKT_AVAILABLE;
                }
            }
        }
        if(m_state==PKT_AVAILABLE){
            break;
        }
    }
    return m_state;
}

static transport_layer_t m_transport_layer = {.get_rx_pkt = get_rx_pkt, .send_tx_pkt = send_tx_pkt, .poll = poll};
transport_layer_t *serial_command_transport_init(serial_funcs_t *fncs)
{
    m_serial_func = *fncs;
    return &m_transport_layer;
}