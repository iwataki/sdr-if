/*
 * vfo_control.c
 *
 *  Created on: 2020/03/22
 *      Author: �@��Y
 */
#include "vfo_control.h"
#include "hardware.h"
#include "system_constants.h"
#include "si5351.h"

void apply_vfo_state(AUX_VFO_OUTPUT_STATE_t*vfo,CALIBRATION_DATA_t*cal){
	SI5351_CONFIG_t cfg;
	cfg.xtal_freq=cal->crystal;
	set_frequency(&cfg,OSC_BFO_CHANNEL,cal->bfo_freq);
	enable_clk(OSC_BFO_CHANNEL);

	if(vfo->enable){
		set_frequency(&cfg,OSC_AUX_VFO_CHANNEL,vfo->freqency);
		enable_clk(OSC_AUX_VFO_CHANNEL);
	}else{
		disable_clk(OSC_AUX_VFO_CHANNEL);
	}
}
