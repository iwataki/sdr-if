/*! \file i2ceeprom.h \brief Interface for standard I2C EEPROM memories. */
//*****************************************************************************
//
// File Name	: 'i2ceeprom.h'
// Title		: Interface for standard I2C EEPROM memories
// Author		: Pascal Stang - Copyright (C) 2003
// Created		: 2003.04.23
// Revised		: 2003.04.23
// Version		: 0.1
// Target MCU	: Atmel AVR series
// Editor Tabs	: 4
//
///	\ingroup driver_hw
/// \defgroup i2ceeprom Interface for standard I2C EEPROM memories (i2ceeprom.c)
/// \code #include "i2ceeprom.h" \endcode
/// \par Overview
///		This library provides functions for reading and writing standard
///	24Cxxx/24LCxxx I2C EEPROM memories.  Memory sizes up to 64Kbytes are
///	supported.  Future revisions may include page-write support.
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************

#ifndef __I2CEEPROM_H__
#define __I2CEEPROM_H__

#define SEND_START_COND 0xa4
#define SEND_STOP_COND 0x94
#define I2C_TRANSACTION_START 0x84
#include <stdint.h>

// functions

//! Initialize I2C interface
void i2c_init(void);
//! send I2C data
int i2c_send(uint8_t device_addr,int size,unsigned char*data,uint8_t is_generate_stop);
//! receive I2C data
int i2c_receive(uint8_t device_addr,int size,unsigned char*data);
#endif
