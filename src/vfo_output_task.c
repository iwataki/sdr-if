/*
 * normal_vfo_output_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */


#include "hardware.h"
#include "vfo_output_task.h"
#include "system_constants.h"
#include "si5351.h"
#include "vfo_control.h"

static SYSTEM_STATUS_t*status;
static AUX_VFO_OUTPUT_STATE_t previous_vfo_config;
static CALIBRATION_DATA_t previous_calibration_state;

void copy_state(SYSTEM_STATUS_t*origin,AUX_VFO_OUTPUT_STATE_t*vfo,CALIBRATION_DATA_t*cal){
	*cal=origin->calibration;
	*vfo=origin->aux_vfo_status;
}


void vfo_output_task_init(SYSTEM_STATUS_t*s){
	status=s;
	copy_state(s,&previous_vfo_config,&previous_calibration_state);
	apply_vfo_state(&previous_vfo_config,&previous_calibration_state);
}
void vfo_output_task(){
	AUX_VFO_OUTPUT_STATE_t vfo;
	CALIBRATION_DATA_t cal;
	copy_state(status,&vfo,&cal);
	if((aux_vfo_output_state_compare(&vfo,&previous_vfo_config)!=0)
			||(calibration_data_compare(&cal,&previous_calibration_state)!=0)){
		apply_vfo_state(&vfo,&cal);
		previous_vfo_config=vfo;
		previous_calibration_state=cal;

	}
}
