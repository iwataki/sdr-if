#include "modulation_mode_control_task.h"
#include "SignalProcessing/sdr_common.h"

static SYSTEM_STATUS_t *s;
static RADIO_CIRCUIT_POWER_STATE_t prev_txrx_state;
static MODULATION_MODE_t prev_modulation_mode;

static void update_modulation_mode(RADIO_CIRCUIT_POWER_STATE_t *pwr_state, MODULATION_MODE_t mod)
{
    if(s->operation_default_mode){
        if(mod==MOD_FM&&pwr_state->is_receiver_enable){
            s->squelch_enable=1;
        }else{
            s->squelch_enable=0;
        }
    }
    if (pwr_state->is_receiver_enable)
    {
        switch (mod)
        {
        case MOD_SSB_USB:
            sdr_set_modulation_mode(SDR_USB_RX);
            break;
        case MOD_SSB_LSB:
            sdr_set_modulation_mode(SDR_LSB_RX);
            break;
        case MOD_AM:
            sdr_set_modulation_mode(SDR_AM_RX);
            break;
        case MOD_FM:
            sdr_set_modulation_mode(SDR_FM_RX);
            break;
        case MOD_CW:
            sdr_set_modulation_mode(SDR_CW_RX);
            break;
        default:
            return;
            break;
        }
    }
    else if (pwr_state->is_transmitter_enable)
    {
        switch (mod)
        {
        case MOD_SSB_USB:
            sdr_set_modulation_mode(SDR_USB_TX);
            break;
        case MOD_SSB_LSB:
            sdr_set_modulation_mode(SDR_LSB_TX);
            break;
        case MOD_AM:
            sdr_set_modulation_mode(SDR_AM_TX);
            break;
        case MOD_FM:
            sdr_set_modulation_mode(SDR_FM_TX);
            break;
        case MOD_CW:
            sdr_set_modulation_mode(SDR_CW_TX);
            break;
        default:
            return;
            break;
        }
    }
    else
    {
        //no operation
        return;
    }
}
void modulation_mode_control_task()
{
    if (radio_circuit_power_state_compare(&prev_txrx_state, &s->radio_circuit_status) || s->modulation_mode != prev_modulation_mode)
    {
        update_modulation_mode(&s->radio_circuit_status, s->modulation_mode);
        prev_modulation_mode = s->modulation_mode;
        prev_txrx_state = s->radio_circuit_status;
    }
}
void modulation_mode_control_task_init(SYSTEM_STATUS_t *status)
{
    s = status;
    prev_modulation_mode = s->modulation_mode;
    prev_txrx_state = s->radio_circuit_status;
    update_modulation_mode(&s->radio_circuit_status, s->modulation_mode);
}