#include "controlpin_observe_task.h"
#include "hardware.h"

static SYSTEM_STATUS_t*s;

void controlpin_observe_task_init(SYSTEM_STATUS_t*status){
    s=status;
}
static MODULATION_MODE_t mode_table[]={
    MOD_SSB_USB,
    MOD_CW,
    MOD_AM,
    MOD_FM
};
void controlpin_observe_task(void){
    int modepinval=~(get_switch(&modesel_0)|get_switch(&modesel_1)<<1)&0x03;
    s->modulation_mode=mode_table[modepinval];
    if(get_switch(&tx_enable_in)){
        s->radio_circuit_status.is_receiver_enable=0;
        s->radio_circuit_status.is_transmitter_enable=1;
    }else if(get_switch(&rx_enable_in)){
        s->radio_circuit_status.is_receiver_enable=1;
        s->radio_circuit_status.is_transmitter_enable=0;
    }
}