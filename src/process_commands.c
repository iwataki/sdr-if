#include "process_commands.h"
#include "commands.h"
#include "stdint_serialize.h"

static transport_layer_t *t;
void command_processor_init(transport_layer_t *transp)
{
    t = transp;
}

void command_processor_poll()
{
    TRANSPORT_LAYER_STATE_t transport_state = t->poll();
    if (transport_state == PKT_AVAILABLE)
    {
        uint8_t *data;
        size_t pkt_size;
        uint32_t retval;
        t->get_rx_pkt(&data, &pkt_size);
        if (data[0] != COMMAND_PKT)
        {
            return; //not handle here
        }
        retval = commands_dispatch(data[1], data + 2, pkt_size - 2);
        uint8_t resp[2 + sizeof(int32_t)];
        resp[0] = RESPONSE_PKT;
        resp[1] = data[0];
        int32_encode(resp + 2, retval);
        t->send_tx_pkt(resp, 2 + sizeof(int32_t));
    }
}