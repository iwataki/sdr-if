/*
 * adc.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#include "adc.h"
static void gpio_init(GPIO_TypeDef*gpio,uint32_t pin){
	uint32_t rcc_gpio=RCC_AHBPeriph_GPIOA;
	if(gpio==GPIOA){
		rcc_gpio=RCC_AHBPeriph_GPIOA;
	}else if(gpio==GPIOB){
		rcc_gpio=RCC_AHBPeriph_GPIOB;
	}

	RCC_AHBPeriphClockCmd(rcc_gpio,ENABLE);
	GPIO_InitTypeDef gpioinit;
	GPIO_StructInit(&gpioinit);
	gpioinit.GPIO_Mode=GPIO_Mode_AN;
	gpioinit.GPIO_OType=GPIO_OType_PP;
	gpioinit.GPIO_Pin=pin;
	gpioinit.GPIO_PuPd=GPIO_PuPd_NOPULL;
	gpioinit.GPIO_Speed=GPIO_Speed_2MHz;
	GPIO_Init(gpio,&gpioinit);

}
static void adc_init_int(ADC_TypeDef*adc){
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12,ENABLE);
	ADC_VoltageRegulatorCmd(adc, ENABLE);
    //delay

    ADC_CommonInitTypeDef adc_common_init;
    adc_common_init.ADC_Mode = ADC_Mode_Independent;
    adc_common_init.ADC_Clock = ADC_Clock_AsynClkMode;
    adc_common_init.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    adc_common_init.ADC_DMAMode = ADC_DMAMode_Circular;
    adc_common_init.ADC_TwoSamplingDelay = 0;

    ADC_CommonInit(adc, &adc_common_init);
    //set inittype
    ADC_InitTypeDef inittype;
    ADC_StructInit(&inittype);
    inittype.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Disable;
    inittype.ADC_Resolution = ADC_Resolution_12b;
    inittype.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_0;
    inittype.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_None;//software trigger
    inittype.ADC_DataAlign = ADC_DataAlign_Right;
    inittype.ADC_OverrunMode = ADC_OverrunMode_Disable;
    inittype.ADC_AutoInjMode = ADC_AutoInjec_Disable;
    inittype.ADC_NbrOfRegChannel = 1;
    ADC_Init(adc, &inittype);


    ADC_Cmd(adc, ENABLE);
}

void adc_init(ADC_t*adc,ADC_TypeDef*adc_instance, GPIO_TypeDef*gpio,uint32_t gpio_pin,uint32_t adc_channel){
	adc->adc=adc_instance;
	adc->gpio=gpio;
	adc->gpio_pin=gpio_pin;
	adc->adc_channel=adc_channel;
	gpio_init(gpio,gpio_pin);
	adc_init_int(adc_instance);


}
uint16_t adc_get(ADC_t*adc){
	ADC_RegularChannelConfig(adc->adc,adc->adc_channel,1,ADC_SampleTime_7Cycles5);
	while(!ADC_GetFlagStatus(adc->adc, ADC_FLAG_RDY));
	ADC_StartConversion(adc->adc);
	while(!ADC_GetFlagStatus(adc->adc, ADC_FLAG_EOC));
	return ADC_GetConversionValue(adc->adc);
}
