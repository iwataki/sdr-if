#include "stdint_serialize.h"

uint8_t uint8_decode(uint8_t*data){
    return *data;
}
int8_t int8_decode(uint8_t*data){
    return (int8_t)*data;
}

uint16_t uint16_decode(uint8_t*data){
    return ((uint16_t)data[0])|((uint16_t)data[1]<<8);
}
int16_t int16_decode(uint8_t*data){
    return (int16_t)(((uint16_t)data[0])|((uint16_t)data[1]<<8));
}

uint32_t uint32_decode(uint8_t*data){
    return ((uint32_t)data[0])|((uint32_t)data[1]<<8)|((uint32_t)data[2]<<16)|((uint32_t)data[3]<<24);
}
int32_t int32_decode(uint8_t*data){
    return (int32_t)((uint32_t)data[0])|((uint32_t)data[1]<<8)|((uint32_t)data[2]<<16)|((uint32_t)data[3]<<24);
}

void uint8_encode(uint8_t*data,uint8_t src){
    *data=src;
}
void int8_encode(uint8_t*data,int8_t src){
    *data=src;
}

void uint16_encode(uint8_t*data,uint16_t src){
    data[0]=src&0xff;
    data[1]=(src>>8)&0xff;
}
void int16_encode(uint8_t*data,int16_t src){
    data[0]=src&0xff;
    data[1]=(src>>8)&0xff;
}

void uint32_encode(uint8_t*data,uint32_t src){
    data[0]=src&0xff;
    data[1]=(src>>8)&0xff;
    data[2]=(src>>16)&0xff;
    data[3]=(src>>24)&0xff;
}
void int32_encode(uint8_t*data,int32_t src){
    data[0]=src&0xff;
    data[1]=(src>>8)&0xff;
    data[2]=(src>>16)&0xff;
    data[3]=(src>>24)&0xff;
}