/*
 * main.c
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#include "hardware.h"
#include "timer.h"
#include "application.h"
#include "power_management.h"



void boot(){
	init_hardware();

	init_application();
	timer_init(10,update_hardware);
}

int main(void){
	boot();

	while(1){
		process_application();
		system_idle();
	}
	return 0;
}
