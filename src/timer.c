#include "timer.h"
#include "stm32f30x_conf.h"

void(* handler_internal)(void)=0;

void timer_init(uint16_t period,void(*handler)(void)){
    handler_internal=handler;
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
    RCC_ClocksTypeDef clk;
    RCC_GetClocksFreq(&clk);
    SysTick_Config(clk.HCLK_Frequency *period/1000 );
}

void SysTick_Handler(void){
    if(handler_internal!=0){
        handler_internal();
    }
}