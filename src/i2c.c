/*! \file i2ceeprom.c \brief Interface for standard I2C EEPROM memories. */
//*****************************************************************************
//
// File Name	: 'i2ceeprom.c'
// Title		: Interface for standard I2C EEPROM memories
// Author		: Pascal Stang - Copyright (C) 2003
// Created		: 2003.04.23
// Revised		: 2003.04.23
// Version		: 0.1
// Target MCU	: Atmel AVR series
// Editor Tabs	: 4
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************

#include "stm32f30x_conf.h"
#include "i2c.h"

#define I2C_INSTANCE_COUNT 1
typedef struct
{
	I2C_TypeDef *i2c;
	GPIO_TypeDef *gpio;
	uint32_t sda_gpio_pin;
	uint32_t scl_gpio_pin;
	uint32_t i2c_rcc;
	uint32_t gpio_rcc;
	uint8_t sda_gpio_pinsource;
	uint8_t scl_gpio_pinsource;
	uint8_t evt_irq_ch;
	uint8_t err_irq_ch;
} i2c_resource_map_t;

static i2c_resource_map_t i2c_resources[I2C_INSTANCE_COUNT] = {
	{I2C1, GPIOB, GPIO_Pin_6, GPIO_Pin_7, RCC_APB1Periph_I2C1, RCC_AHBPeriph_GPIOB, GPIO_PinSource6, GPIO_PinSource7, I2C1_EV_IRQn, I2C1_ER_IRQn}};

static i2c_resource_map_t *get_i2c_resource(uint8_t ch)
{
	return &i2c_resources[ch];
}
// Standard I2C bit rates are:
// 100KHz for slow speed
// 400KHz for high speed

// functions
void i2c_init(void)
{
	i2c_resource_map_t *resource = get_i2c_resource(0);
	RCC_AHBPeriphClockCmd(resource->gpio_rcc, ENABLE);
	GPIO_InitTypeDef gpio_init_data;
	GPIO_StructInit(&gpio_init_data);
	gpio_init_data.GPIO_Mode = GPIO_Mode_AF;
	gpio_init_data.GPIO_OType = GPIO_OType_OD;
	gpio_init_data.GPIO_PuPd = GPIO_PuPd_NOPULL;
	gpio_init_data.GPIO_Speed = GPIO_Speed_2MHz;
	gpio_init_data.GPIO_Pin = resource->sda_gpio_pin | resource->scl_gpio_pin;
	GPIO_Init(resource->gpio, &gpio_init_data);
	GPIO_PinAFConfig(resource->gpio, resource->sda_gpio_pinsource, GPIO_AF_4);
	GPIO_PinAFConfig(resource->gpio, resource->scl_gpio_pinsource, GPIO_AF_4);
	RCC_I2CCLKConfig(RCC_I2C1CLK_SYSCLK);
	RCC_APB1PeriphClockCmd(resource->i2c_rcc, ENABLE);
	I2C_InitTypeDef i2c_init_data;
	I2C_StructInit(&i2c_init_data);
	i2c_init_data.I2C_Timing = 0x20705C89;
	i2c_init_data.I2C_AnalogFilter = I2C_AnalogFilter_Enable;
	i2c_init_data.I2C_DigitalFilter = 0x08;
	i2c_init_data.I2C_Mode = I2C_Mode_I2C;
	i2c_init_data.I2C_Ack = I2C_Ack_Enable;
	I2C_Init(resource->i2c, &i2c_init_data);
}
static uint32_t determine_reload_end_mode(int transfer_size, uint8_t is_generate_stop)
{
	if (transfer_size > 255)
	{
		return I2C_Reload_Mode;
	}
	else if (is_generate_stop)
	{
		return I2C_AutoEnd_Mode;
	}
	return I2C_SoftEnd_Mode;
}

int i2c_send(uint8_t device_addr, int size, unsigned char *data, uint8_t is_generate_stop)
{
	i2c_resource_map_t *resource = get_i2c_resource(0);
	int remain = size;
	uint8_t block_size = (remain > 255) ? 255 : remain;
	I2C_TransferHandling(resource->i2c, device_addr, block_size, determine_reload_end_mode(remain, is_generate_stop), I2C_Generate_Start_Write);
	int block_count = 0;
	int i = 0;
	while (remain > 0)
	{
		for (i = 0; i < block_size; i++)
		{
			while ((I2C_GetFlagStatus(resource->i2c, I2C_FLAG_TXIS) == RESET) && (I2C_GetFlagStatus(resource->i2c, I2C_FLAG_NACKF) == RESET))
				;
			if (I2C_GetFlagStatus(resource->i2c, I2C_FLAG_NACKF))
			{
				goto TX_ABORT; //device returned NACK;
			}
			I2C_SendData(resource->i2c, data[block_count * 255 + i]);
		}
		block_count++;
		remain -= block_size;
		block_size = (remain > 255) ? 255 : remain;
		if (I2C_GetFlagStatus(resource->i2c, I2C_FLAG_TC) == SET)
		{
			break;
		}
		if (I2C_GetFlagStatus(resource->i2c, I2C_FLAG_TCR) == SET)
		{
			I2C_TransferHandling(resource->i2c, device_addr, block_size, determine_reload_end_mode(remain, is_generate_stop), I2C_No_StartStop);
		}
	}
TX_ABORT:
	return block_count * 255 + i;
}
int i2c_receive(uint8_t device_addr, int size, unsigned char *data)
{
	i2c_resource_map_t *resource = get_i2c_resource(0);
	int remain = size;
	uint8_t block_size = (remain > 255) ? 255 : remain;
	I2C_TransferHandling(resource->i2c, device_addr, block_size, determine_reload_end_mode(remain, 1), I2C_Generate_Start_Read);
	int block_count = 0;
	int i = 0;
	while (remain > 0)
	{
		for (i = 0; i < block_size; i++)
		{
			while (I2C_GetFlagStatus(resource->i2c, I2C_FLAG_RXNE) == RESET)
				;
			data[block_count * 255 + i]=I2C_ReceiveData(resource->i2c);
		}
		block_count++;
		remain -= block_size;
		block_size = (remain > 255) ? 255 : remain;
		if (I2C_GetFlagStatus(resource->i2c, I2C_FLAG_TC) == SET)
		{
			break;
		}
		if (I2C_GetFlagStatus(resource->i2c, I2C_FLAG_TCR) == SET)
		{
			I2C_TransferHandling(resource->i2c, device_addr, block_size, determine_reload_end_mode(remain, 1), I2C_No_StartStop);
		}
	}
	return block_count * 255 + i;
}
