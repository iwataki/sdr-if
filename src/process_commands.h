#pragma once
#include <stdint.h>
#include <stddef.h>
#include "transport_layer_def.h"


void command_processor_init(transport_layer_t*transp);

void command_processor_poll();