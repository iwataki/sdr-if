/*
 * application.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef APPLICATION_H_
#define APPLICATION_H_
#include <inttypes.h>
#include "task.h"

typedef struct {
	int controlpin_input_observe_task_id;
	int modulation_mode_observe_task_id;
	int command_process_task_id;
	int vfo_output_task_id;
	int radio_circuit_ctrl_task_id;
	int ptt_observe_normal_task_id;
	int rssi_observe_task_id;
	int sql_ctrl_task_id;
}APPLICATION_TASK_ID_TABLE_t;
void switch_to_calibration_mode(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids);
void switch_to_normal_mode(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids);
void init_application(void);
void process_application(void);

#endif /* APPLICATION_H_ */
