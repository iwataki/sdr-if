#pragma once
#include "transport_layer_def.h"

typedef struct{
    int (*is_rx_char_available)(void);
    uint8_t(*get_char)(void);
    void(*put_char)(uint8_t);
}serial_funcs_t;

transport_layer_t*serial_command_transport_init(serial_funcs_t*fncs);