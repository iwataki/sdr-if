/*
 * data_structure.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#include "data_structure.h"


uint8_t calibration_data_compare(CALIBRATION_DATA_t*d1,CALIBRATION_DATA_t*d2){
	if(d1->bfo_freq!=d2->bfo_freq)return 1;
	if(d1->crystal!=d2->crystal)return 1;
	return 0;
}

uint8_t radio_circuit_power_state_compare(RADIO_CIRCUIT_POWER_STATE_t*d1,RADIO_CIRCUIT_POWER_STATE_t*d2){
	if(d1->is_receiver_enable!=d2->is_receiver_enable)return 1;
	if(d1->is_transmitter_enable!=d2->is_transmitter_enable)return 1;
	return 0;
}

uint8_t aux_vfo_output_state_compare(AUX_VFO_OUTPUT_STATE_t*s1,AUX_VFO_OUTPUT_STATE_t*s2){
	if(s1->freqency!=s2->freqency)return 1;
	if(s1->enable!=s2->enable)return 1;
	return 0;
}

uint8_t system_status_compare(SYSTEM_STATUS_t*s1,SYSTEM_STATUS_t*s2){
	if(calibration_data_compare(&s1->calibration,&s2->calibration)) return 1;
	if(radio_circuit_power_state_compare(&s1->radio_circuit_status,&s2->radio_circuit_status)) return 1;
	if(aux_vfo_output_state_compare(&s1->aux_vfo_status,&s2->aux_vfo_status))return 1;
	if(s1->modulation_mode!=s2->modulation_mode)return 1;
	if(s1->rssi!=s2->rssi)return 1;
	if(s1->squelch_level!=s2->squelch_level)return 1;
	if(s1->squelch_enable!=s2->squelch_enable)return 1;
	if(s1->operation_default_mode!=s2->operation_default_mode)return 1;
	return 0;
}