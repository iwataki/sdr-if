#include "sqeulch_control_task.h"
#include "hardware.h"
#include "SignalProcessing/sdr_common.h"
#include "audio_output.h"

static SYSTEM_STATUS_t *s;
static uint8_t m_mute_state = 0;

void sql_ctrl_task_init(SYSTEM_STATUS_t *st)
{
    s = st;
}
void sql_ctrl_task(void)
{
    s->squelch_level = adc_get(&sql_level_in);
    uint8_t mute_state = 0;
    if (s->radio_circuit_status.is_receiver_enable)
    {
        if (s->squelch_enable && (s->rssi < s->squelch_level))
        {
            mute_state = 1;
        }else{
            mute_state = 0;
        }
        if(m_mute_state!=mute_state){
            m_mute_state=mute_state;
            if(m_mute_state){
                audio_output_disable(SDR_RX_SPK_DAC_CH);
            }else{
                audio_output_enable(SDR_RX_SPK_DAC_CH);
            }
        }
    }
}