/*
 * radio_circuit_control.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef RADIO_CIRCUIT_CONTROL_TASK_H_
#define RADIO_CIRCUIT_CONTROL_TASK_H_
#include "data_structure.h"

void radio_circuit_control_task_init(SYSTEM_STATUS_t*s);
void radio_circuit_control_task(void);


#endif /* RADIO_CIRCUIT_CONTROL_TASK_H_ */
