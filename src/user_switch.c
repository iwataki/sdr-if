/*
 * user_switch.c
 *
 *  Created on: 2016/11/01
 *      Author: �@��Y
 */
#include "user_switch.h"
#include "gpio.h"
void init_usersw(USERSW_t *sw)
{
	gpio_enable(sw->gpio);
	GPIO_InitTypeDef init;
	GPIO_StructInit(&init);
	init.GPIO_Mode = GPIO_Mode_IN;
	init.GPIO_OType = GPIO_OType_OD;
	init.GPIO_Pin = sw->pin;
	init.GPIO_PuPd = GPIO_PuPd_UP;
	init.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(sw->gpio, &init);
	sw->latest_state = GPIO_ReadInputDataBit(sw->gpio, sw->pin);
}
void update_switch(USERSW_t *sw)
{
	sw->previous_state = sw->latest_state;
	sw->latest_state = GPIO_ReadInputDataBit(sw->gpio, sw->pin);
	if (sw->latest_state == 0)
	{
		if (sw->previous_state == 0)
		{
			sw->latest_event = USERSW_HOLD;
		}
		else
		{
			sw->latest_event = USERSW_PUSHED;
		}
	}
	else
	{
		if (sw->previous_state == 0)
		{
			sw->latest_event = USERSW_RELEASED;
		}
		else
		{
			sw->latest_event = USERSW_RELEASE;
		}
	}
}
int get_switch(USERSW_t *sw)
{

	if (sw->latest_state == 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int get_switch_event(USERSW_t *sw)
{
	int event = sw->latest_event;
	sw->latest_event = USERSW_NO_EVENT;
	return event;
}
