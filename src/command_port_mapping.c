#include "command_port_mapping.h"
#include "usart.h"

static int usart1_check(void){
    return usart_is_received(1);
}

static uint8_t usart1_get(void){
    return usart_getc_noblock(1);
}
static void usart1_put(uint8_t c){
    usart_putc(1,c);
}


static serial_funcs_t m_ser_func={.is_rx_char_available=usart1_check,.get_char=usart1_get,.put_char=usart1_put};
serial_funcs_t* get_serial_funcs(int ch){
    return &m_ser_func;
}