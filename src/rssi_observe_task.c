/*
 * rssi_observe_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */
#include "rssi_observe_task.h"
#include "hardware.h"
#include "dac.h"
#include "SignalProcessing/sdr_common.h"

static SYSTEM_STATUS_t *state;
static uint16_t prev_rssi = 0;
void rssi_observe_task_init(SYSTEM_STATUS_t *s)
{
	state = s;
}
void rssi_observe_task(void)
{
	state->rssi=sdr_get_rssi();
	if (state->radio_circuit_status.is_receiver_enable)
	{
		if (prev_rssi != state->rssi)
		{
			prev_rssi = state->rssi;
			dac_set(prev_rssi);
		}
	}
	else
	{
		prev_rssi = 0;
		dac_set(0);
	}
}
