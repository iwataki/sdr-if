#pragma once
#include <stdint.h>
#include <stddef.h>

typedef void(*read_callback_t)(uint16_t*,size_t);

void audio_input_capture_init(uint32_t samplingrate,uint16_t*buffer,uint16_t size);
//return 0 success
int8_t audio_input_capture_start(uint8_t ch,read_callback_t cb);
int8_t audio_input_capture_stop(uint8_t ch);