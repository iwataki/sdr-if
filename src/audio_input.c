#include "audio_input.h"
#include "stm32f30x_conf.h"

static read_callback_t m_read_callback;
static size_t m_buffer_size;
static uint16_t* mp_buffer;
typedef struct{
    uint32_t rcc_gpio;
    GPIO_TypeDef*gpio;
    uint16_t pin;
    uint8_t adc_pin_no;

}adc_no_gpio_map_item_t;
static const adc_no_gpio_map_item_t adc_no_gpio_map[]={
    {RCC_AHBPeriph_GPIOA,GPIOA,GPIO_Pin_0,1},
    {RCC_AHBPeriph_GPIOA,GPIOA,GPIO_Pin_1,2},
    {RCC_AHBPeriph_GPIOA,GPIOA,GPIO_Pin_2,3},
    {RCC_AHBPeriph_GPIOA,GPIOA,GPIO_Pin_3,4},
    {RCC_AHBPeriph_GPIOC,GPIOC,GPIO_Pin_0,6},
    {RCC_AHBPeriph_GPIOC,GPIOC,GPIO_Pin_1,7},
    {RCC_AHBPeriph_GPIOC,GPIOC,GPIO_Pin_2,8},
    {RCC_AHBPeriph_GPIOC,GPIOC,GPIO_Pin_3,9},
    {RCC_AHBPeriph_GPIOB,GPIOB,GPIO_Pin_0,11},
    {RCC_AHBPeriph_GPIOB,GPIOB,GPIO_Pin_1,12},
    {RCC_AHBPeriph_GPIOB,GPIOB,GPIO_Pin_13,13},
    {0,0,0,0}
};

static void init_adc()
{
    RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div2);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);

    ADC_VoltageRegulatorCmd(ADC1, ENABLE);
    //delay

    ADC_CommonInitTypeDef adc_common_init;
    adc_common_init.ADC_Mode = ADC_Mode_Independent;
    adc_common_init.ADC_Clock = ADC_Clock_AsynClkMode;
    adc_common_init.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
    adc_common_init.ADC_DMAMode = ADC_DMAMode_Circular;
    adc_common_init.ADC_TwoSamplingDelay = 0;

    ADC_CommonInit(ADC1, &adc_common_init);
    //set inittype
    ADC_InitTypeDef inittype;
    ADC_StructInit(&inittype);
    inittype.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Disable;
    inittype.ADC_Resolution = ADC_Resolution_12b;
    inittype.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_9; //TIM1_TRGO (see refernce manual Table 90)
    inittype.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_RisingEdge;
    inittype.ADC_DataAlign = ADC_DataAlign_Right;
    inittype.ADC_OverrunMode = ADC_OverrunMode_Disable;
    inittype.ADC_AutoInjMode = ADC_AutoInjec_Disable;
    inittype.ADC_NbrOfRegChannel = 1;
    ADC_Init(ADC1, &inittype);
    ADC_DMAConfig(ADC1,ADC_DMAMode_Circular);
    ADC_DMACmd(ADC1, ENABLE);

    ADC_Cmd(ADC1, ENABLE);
}

static void init_timer(uint32_t samplingrate)
{
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
    TIM_TimeBaseInitTypeDef timebaseinit;
    TIM_TimeBaseStructInit(&timebaseinit);
    timebaseinit.TIM_CounterMode = TIM_CounterMode_Up;
    timebaseinit.TIM_ClockDivision = TIM_CKD_DIV1;
    timebaseinit.TIM_RepetitionCounter = 0;
    timebaseinit.TIM_Prescaler=0;
    timebaseinit.TIM_Period = SystemCoreClock/samplingrate;
    TIM_TimeBaseInit(TIM1, &timebaseinit);
    TIM_SelectOutputTrigger(TIM1, TIM_TRGOSource_Update);
    TIM_InternalClockConfig(TIM1);
    
}

static void init_dma(uint16_t *buffer,size_t buflen)
{
    NVIC_InitTypeDef nvic_init;
    nvic_init.NVIC_IRQChannelCmd=ENABLE;
    nvic_init.NVIC_IRQChannel=DMA1_Channel1_IRQn;
    nvic_init.NVIC_IRQChannelPreemptionPriority=0;
    nvic_init.NVIC_IRQChannelSubPriority=0;
    NVIC_Init(&nvic_init);
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1,ENABLE);
    DMA_InitTypeDef dmainit;
    DMA_StructInit(&dmainit);
    dmainit.DMA_PeripheralBaseAddr=(uint32_t)&(ADC1->DR);
    dmainit.DMA_MemoryBaseAddr=(uint32_t)buffer;
    dmainit.DMA_DIR = DMA_DIR_PeripheralSRC; //adc data regster -> buffer
    dmainit.DMA_BufferSize=buflen;
    dmainit.DMA_PeripheralInc=DMA_PeripheralInc_Disable;
    dmainit.DMA_MemoryInc= DMA_MemoryInc_Enable;
    dmainit.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Word;
    dmainit.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
    dmainit.DMA_Mode=DMA_Mode_Circular;
    dmainit.DMA_Priority = DMA_Priority_High;
    DMA_Init(DMA1_Channel1,&dmainit);
    DMA_ITConfig(DMA1_Channel1,DMA_IT_HT|DMA_IT_TC,ENABLE);
    DMA_Cmd(DMA1_Channel1,ENABLE);
}

static const adc_no_gpio_map_item_t* get_adc_gpio_map(uint8_t chan){
    for(int i=0;adc_no_gpio_map[i].adc_pin_no!=0;i++){
        if(chan==adc_no_gpio_map[i].adc_pin_no){
            return &adc_no_gpio_map[i];
        }
    }
    return 0;
}

static int8_t set_gpio_as_analogin(uint8_t chan){
    const adc_no_gpio_map_item_t*gpio_map=get_adc_gpio_map(chan);
    if(gpio_map==0){
        return -1;
    }
    RCC_AHBPeriphClockCmd(gpio_map->rcc_gpio,ENABLE);
    GPIO_InitTypeDef gpio_init_data;
    GPIO_StructInit(&gpio_init_data);
    gpio_init_data.GPIO_Mode=GPIO_Mode_AN;
    gpio_init_data.GPIO_PuPd=GPIO_PuPd_NOPULL;
    gpio_init_data.GPIO_Speed=GPIO_Speed_2MHz;
    gpio_init_data.GPIO_Pin=gpio_map->pin;
    GPIO_Init(gpio_map->gpio,&gpio_init_data);
    return 0;
}
static int8_t gpio_free(uint8_t chan){
    const adc_no_gpio_map_item_t*gpio_map=get_adc_gpio_map(chan);
    if(gpio_map==0){
        return -1;
    }
    GPIO_InitTypeDef gpio_init_data;
    GPIO_StructInit(&gpio_init_data);
    gpio_init_data.GPIO_Pin=gpio_map->pin;
    GPIO_Init(gpio_map->gpio,&gpio_init_data);
    return 0;
}

void audio_input_capture_init(uint32_t samplingrate,uint16_t*buffer,uint16_t size)
{
    init_adc();
    init_timer(samplingrate);
    init_dma(buffer,size);
    mp_buffer=buffer;
    m_buffer_size = size;
    m_read_callback=0;
}
int8_t audio_input_capture_start(uint8_t chan, read_callback_t cb)
{
    if (ADC_GetStartConversionStatus(ADC1) == SET)
    {
        return -1; //busy
    }
    m_read_callback=cb;
    int8_t retcode = set_gpio_as_analogin(chan);
    if(retcode<0){
        return retcode;
    }
    ADC_RegularChannelConfig(ADC1, chan, 1, ADC_SampleTime_7Cycles5);
    ADC_StartConversion(ADC1);
    TIM_Cmd(TIM1,ENABLE);
    return 0;
}
int8_t audio_input_capture_stop(uint8_t chan)
{
    ADC_StopConversion(ADC1);
    return gpio_free(chan);
}

void DMA1_Channel1_IRQHandler(){
    size_t buf_work_size = m_buffer_size/2;
    uint16_t*p_buffer=0;
    if(DMA_GetITStatus(DMA1_IT_TC1)){//latter half of buffer is full
        p_buffer = mp_buffer+buf_work_size;
        DMA_ClearITPendingBit(DMA1_IT_TC1);

    }
    else if(DMA_GetITStatus(DMA1_IT_HT1))//former half of buffer is full
    {
        p_buffer = mp_buffer;
        DMA_ClearITPendingBit(DMA1_IT_HT1);   
    }
    if(m_read_callback&&p_buffer){
        m_read_callback(p_buffer,buf_work_size);
    }
    
}