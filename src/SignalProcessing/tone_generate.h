/*
 * ToneGenerator.h
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#ifndef SRC_SIGNALPROCESSING_TONEGENERATOR_H_
#define SRC_SIGNALPROCESSING_TONEGENERATOR_H_
#include <arm_math.h>

typedef struct{
	q15_t phaseCos;
	int block_size;
	int fs;
}tone_generator_state_t;

void tone_generator_init(tone_generator_state_t*state,int block_size,int sampling_freq);
void tone_generator_process(tone_generator_state_t*state,int freq,q15_t*data,q15_t phase_offset);

#endif /* SRC_SIGNALPROCESSING_TONEGENERATOR_H_ */
