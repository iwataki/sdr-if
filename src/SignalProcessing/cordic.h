#pragma once

//return value range [-pi/2,pi/2] mapped to [-2^31>>1 (2^31-1)>>1]
//point(x,y) :x>0
int atan_rhs(int x, int y);
//arg1 range [-pi/2,pi/2] mapped to [-2^31>>1 (2^31-1)>>1]
//arg2.3 cos(arg1),sin(arg1) result
void cordic_sincos(int theta, int *cos_result, int *sin_result);

//return value range [-pi,pi] mapped to [-2^31 2^31-1]
//point(x,y)
int cordic_atan2(int x, int y);
//arg: INT_MIN(-pi)-INT_MAX(pi)
//retval: INT_MIN(-1)-INT_MAX(1)
int cordic_sin(int x);
int cordic_cos(int x);
