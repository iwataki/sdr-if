/*
 * AMReceive.h
 *
 *  Created on: 2019/07/07
 *      Author: S.Iwataki
 */

#ifndef SRC_SIGNALPROCESSING_AMRECEIVE_H_
#define SRC_SIGNALPROCESSING_AMRECEIVE_H_
#include <arm_math.h>
#include "sdr_common.h"
sdr_signal_processor_t* get_am_rx_module();

#endif /* SRC_SIGNALPROCESSING_AMRECEIVE_H_ */
