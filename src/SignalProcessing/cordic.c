#include "cordic.h"
#include <stdint.h>
#include <limits.h>

static const int cordic_table_length = 16; //32;
static const int cordic_atan_table[]={
536870912,
316933405,
167458907,
85004756,
42667331,
21354465,
10679838,
5340245,
2670163,
1335087,
667544,
333772,
166886,
83443,
41722,
20861};/*,
10430,
5215,
2608,
1304,
652,
326,
163,
81,
41,
20,
10,
5,
3,
1,
1,
0
};*/



int atan_rhs(int x,int y){
	int z=0;
	for(int i=0;i<cordic_table_length;i++){
		int d=(y>0)?-1:1;
		int x_new=x-((y*d)>>i);
		int y_new=y+((x*d)>>i);
		z-=d*cordic_atan_table[i];
		x=x_new;
		y=y_new;
	}
	return z;
}

int cordic_atan2(int x,int y){
	if(x>0){
		return atan_rhs(x,y);
	}else if(x<0){//left half plane
		int theta_temp=atan_rhs(-x,y);
		if(theta_temp>0){
			return INT_MAX-theta_temp;
		}else{
			return INT_MIN-theta_temp;
		}
	}else
	{
		if(y>0){
			return INT_MAX>>1;
		}else{
			return INT_MIN>>1;
		}
	}

}

void cordic_sincos(int theta,int* cos_result,int*sin_result){
	int x=INT_MAX>>3;//cordic_gain
	int y=0;
	int phi_acc=0;
	for(int i=0;i<cordic_table_length;i++){
		int d=(theta-phi_acc>0)?1:-1;
		int x_new=x-((y*d)>>i);
		int y_new=y+((x*d)>>i);
		phi_acc+=d*cordic_atan_table[i];
		x=x_new;
		y=y_new;
	}
	*cos_result=-x;
	*sin_result=-y;
}

int cordic_sin(int theta){
	int ans,dummy;
	if(theta<(INT_MIN>>1)){// 3rd quadrant
		cordic_sincos(INT_MIN-theta,&dummy,&ans);
	}else if(theta<(INT_MAX>>1)){// 4th quadrant or 1st quadrant
		cordic_sincos(theta,&dummy,&ans);
	}else{//2nd quadrant
		cordic_sincos(INT_MAX-theta,&dummy,&ans);
	}
	return ans;
}

int cordic_cos(int theta){
	int ans,dummy;
	if(theta<(INT_MIN>>1)){// 3rd quadrant
		cordic_sincos(INT_MIN-theta,&ans,&dummy);
		ans=-ans;
	}else if(theta<(INT_MAX>>1)){// 4th quadrant or 1st quadrant
		cordic_sincos(theta,&ans,&dummy);
	}else{//2nd quadrant
		cordic_sincos(INT_MAX-theta,&ans,&dummy);
		ans=-ans;
	}
	return ans;
}

