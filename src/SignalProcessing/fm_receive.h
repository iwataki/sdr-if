/*
 * FMReceive.h
 *
 *  Created on: 2020/01/18
 *      Author: �@��Y
 */

#ifndef SRC_SIGNALPROCESSING_FMRECEIVE_H_
#define SRC_SIGNALPROCESSING_FMRECEIVE_H_
#include "sdr_common.h"

sdr_signal_processor_t*get_fm_rx_module(void);

#endif /* SRC_SIGNALPROCESSING_FMRECEIVE_H_ */
