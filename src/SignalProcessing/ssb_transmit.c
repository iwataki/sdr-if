/*
 * SSBTransmit.cpp
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#include "ssb_transmit.h"
#include "util.h"
#include "signal_buffer_manage.h"
#include <arm_math.h>


/**
	 * hilbert filter coeffs
	 * N=64
	 * lower edge normalized freq 0.02
	 * higer edge normalized freq 0.48
	 */
#define HILBERT_TAP_NUM 128
static q15_t hilbert_taps[HILBERT_TAP_NUM] = {
	0, 0, 0, 0, 0, 0, 0, 3, 5, 0, -1, 7, 16, 9, -1, 10,
	34, 31, 4, 8, 53, 69, 24, 2, 63, 118, 67, -5, 49, 163, 135, -3, 0, 174, 220,
	21, -88, 114, 291, 80, -204, -54, 294, 172, -320, -358, 154, 266, -400, -811, -232, 295, -413, -1429, -1036,
	113, -342, -2335, -2784, -731, -193, -4618, -10198, -9124, 0, 9105, 10158, 4591, 191, 724, 2750, 2302, 336, -111, 1015,
	1398, 403, -287, 225, 787, 387, -257, -148, 344, 306, -164, -281, 51, 193, -76, -274, -108, 83, -20, -205, -163, 0, 3, -125,
	-150, -45, 4, -61, -107, -57, -1, -21, -61, -47, -7, -3, -27, -29, -8, 1, -8, -13, -5, 1, 0, -4, -2, 0, 0, 0, 0, 0, 0};
static q15_t delay_taps[HILBERT_TAP_NUM] = {
	0, 0, 0, 0, 0, 0, 3, 4, 1, 0, 6, 12, 5, -4, 4, 22, 14, -12, -8, 25, 28, -19, -40, 9,
	42, -20, -90, -43, 40, -12, -149, -144, 0, 4, -199, -292, -107, 14, -215, -466,
	-305, -12, -176, -621, -598, -118, -73, -690, -967, -359, 83, -586, -1364,
	-810, 259, -179, -1719, -1644, 412, 937, -1965, -3984, 501, 9583, 14332,
	9564, 499, -3960, -1949, 928, 408, -1621, -1692, -176, 254, -792, -1331,
	-571, 80, -348, -936, -666, -70, -113, -574, -594, -168, -11, -290, -442, -203,
	13, -101, -274, -186, 4, 0, -133, -138, -11, 37, -39, -82, -18, 38, 8, -36, -17, 25, 22, -7, -10,
	12, 19, 4, -4, 4, 10, 5, 0, 0, 3, 2, 0, 0, 0, 0, 0};
/**
	 * decimation factor
	 * */
#define DECIMATION_FACTOR 4
#define SSB_TX_LSB 0
#define SSB_TX_USB 1

typedef struct
{
	/** filter instances
	 * it holds dynamically allocated memory
	 * don't forget to delete them!!*/
	arm_fir_decimate_instance_q15 input_lpf_and_decimetor;
	arm_fir_instance_q15 hilbert_filter;
	arm_fir_instance_q15 delay_line;
	arm_fir_interpolate_instance_q15 result_interporator_l;
	arm_fir_interpolate_instance_q15 result_interporator_r;

	/*buffers*/
	q15_t *l_ch_workbuffer;
	q15_t *r_ch_workbuffer;
	q15_t *input_decimated;
	q15_t *decimated_workbuffer;

	sdr_signal_processor_t sdr_signal_processor;
	/*
	 * Parameters
	 * */
	int blocksize;
	uint8_t sideband;

} ssb_tx_state_t;

static void ssb_tx_init(sdr_signal_processor_t *inst, uint16_t blocksize)
{
	ssb_tx_state_t *instance = container_of(inst, ssb_tx_state_t, sdr_signal_processor);
	instance->blocksize = blocksize;

	int decimated_size = blocksize / DECIMATION_FACTOR;
	instance->l_ch_workbuffer = alloc_buffer(blocksize);
	instance->r_ch_workbuffer = alloc_buffer(blocksize);
	instance->input_decimated = alloc_buffer(decimated_size);
	instance->decimated_workbuffer = alloc_buffer(decimated_size);

	arm_fir_decimate_init_q15(&instance->input_lpf_and_decimetor, BASEBAND_LPF_TAP_NUM, DECIMATION_FACTOR, baseband_lpf_taps, alloc_buffer(BASEBAND_LPF_TAP_NUM + blocksize - 1), blocksize);
	arm_fir_init_q15(&instance->hilbert_filter, HILBERT_TAP_NUM, hilbert_taps, alloc_buffer(HILBERT_TAP_NUM + decimated_size - 1), decimated_size);
	arm_fir_init_q15(&instance->delay_line, HILBERT_TAP_NUM, delay_taps, alloc_buffer(HILBERT_TAP_NUM + decimated_size - 1), decimated_size);
	arm_fir_interpolate_init_q15(&instance->result_interporator_l, DECIMATION_FACTOR, BASEBAND_LPF_TAP_NUM, baseband_lpf_taps, alloc_buffer(decimated_size + BASEBAND_LPF_TAP_NUM / DECIMATION_FACTOR - 1), decimated_size);
	arm_fir_interpolate_init_q15(&instance->result_interporator_r, DECIMATION_FACTOR, BASEBAND_LPF_TAP_NUM, baseband_lpf_taps, alloc_buffer(decimated_size + BASEBAND_LPF_TAP_NUM / DECIMATION_FACTOR - 1), decimated_size);
}

void ssb_tx_process(sdr_signal_processor_t *inst, uint16_t *input, uint16_t size)
{
	ssb_tx_state_t *instance = container_of(inst, ssb_tx_state_t, sdr_signal_processor);
	dc_remove((q15_t*)input,size);
	//modulation
	int decimated_size = instance->blocksize / DECIMATION_FACTOR;
	arm_fir_decimate_q15(&instance->input_lpf_and_decimetor, (q15_t*)input, instance->input_decimated, instance->blocksize);
	arm_fir_q15(&instance->hilbert_filter, instance->input_decimated, instance->decimated_workbuffer, decimated_size);
	arm_fir_interpolate_q15(&instance->result_interporator_l, instance->decimated_workbuffer, instance->l_ch_workbuffer, decimated_size);
	arm_fir_q15(&instance->delay_line, instance->input_decimated, instance->decimated_workbuffer, decimated_size);
	arm_fir_interpolate_q15(&instance->result_interporator_r, instance->decimated_workbuffer, instance->r_ch_workbuffer, decimated_size);

	if (instance->sideband == SSB_TX_LSB)
	{
		for (int i = 0; i < instance->blocksize; i++)
		{
			instance->l_ch_workbuffer[i] = -instance->l_ch_workbuffer[i];
		}
	}
	q15_t *output_buff;
	uint16_t sz;
	audio_output_get_buffer(instance->sdr_signal_processor.output_ch, (uint16_t**)&output_buff, &sz);
	freq_conv(instance->l_ch_workbuffer, instance->r_ch_workbuffer, output_buff, 0, instance->blocksize,1);
	add_offset(output_buff,sz);
}

static void ssb_tx_deinit(sdr_signal_processor_t *inst)
{
	ssb_tx_state_t *instance = container_of(inst, ssb_tx_state_t, sdr_signal_processor);
	free_buffer(instance->decimated_workbuffer);
	free_buffer(instance->delay_line.pState);
	free_buffer(instance->hilbert_filter.pState);
	free_buffer(instance->input_decimated);
	free_buffer(instance->input_lpf_and_decimetor.pState);
	free_buffer(instance->l_ch_workbuffer);
	free_buffer(instance->r_ch_workbuffer);
	free_buffer(instance->result_interporator_l.pState);
	free_buffer(instance->result_interporator_r.pState);
}

ssb_tx_state_t m_ssb_tx_state;
static void adc_dac_channel_set(sdr_signal_processor_t*p){
	p->input_ch=SDR_TX_MIC_ADC_CH;
	p->output_ch=SDR_TX_IF_DAC_CH;
} 

sdr_signal_processor_t *get_ssb_tx_usb_module()
{
	m_ssb_tx_state.sdr_signal_processor.on_input_captured = ssb_tx_process;
	m_ssb_tx_state.sdr_signal_processor.create = ssb_tx_init;
	m_ssb_tx_state.sdr_signal_processor.deinit = ssb_tx_deinit;
	m_ssb_tx_state.sideband = SSB_TX_USB;
	adc_dac_channel_set(&m_ssb_tx_state.sdr_signal_processor);
	return &m_ssb_tx_state.sdr_signal_processor;
}
sdr_signal_processor_t *get_ssb_tx_lsb_module()
{
	m_ssb_tx_state.sdr_signal_processor.on_input_captured = ssb_tx_process;
	m_ssb_tx_state.sdr_signal_processor.create = ssb_tx_init;
	m_ssb_tx_state.sdr_signal_processor.deinit = ssb_tx_deinit;
	m_ssb_tx_state.sideband = SSB_TX_LSB;
	adc_dac_channel_set(&m_ssb_tx_state.sdr_signal_processor);
	return &m_ssb_tx_state.sdr_signal_processor;
}