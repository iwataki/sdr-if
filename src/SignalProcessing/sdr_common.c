/*
 * DSPCommon.cpp
 *
 *  Created on: 2019/07/06
 *      Author: S.Iwataki
 */

#include "SignalProcessing/sdr_common.h"
#include "audio_input.h"
#include "am_receive.h"
#include "am_transmit.h"
#include "fm_receive.h"
#include "fm_transmit.h"
#include "cw_transmit.h"
#include "ssb_receive.h"
#include "ssb_transmit.h"

/*

FIR filter designed with
http://t-filter.appspot.com

sampling frequency: 48000 Hz

fixed point precision: 16 bits

* 0 Hz - 2300 Hz
  gain = 1
  desired ripple = 5 dB
  actual ripple = n/a

* 3600 Hz - 24000 Hz
  gain = 0
  desired attenuation = -60 dB
  actual attenuation = n/a

*/

int16_t baseband_lpf_taps[BASEBAND_LPF_TAP_NUM] =
	{31, 48, 80, 118, 162, 205, 243, 268, 272, 249, 191,
	 97, -33, -193, -374, -560, -732, -869, -949, -951,
	 -860, -666, -367, 30, 508, 1041, 1598, 2142, 2634, 3040,
	 3329, 3479, 3479, 3329, 3040, 2634, 2142, 1598,
	 1041, 508, 30, -367, -666, -860, -951, -949,
	 -869, -732, -560, -374, -193, -33, 97, 191,
	 249, 272, 268, 243, 205, 162, 118, 80, 48, 31};
static q15_t baseband_lpf_state[BASEBAND_LPF_TAP_NUM + BLOCK_SIZE - 1];
arm_fir_instance_q15 baseband_lpf;
static q15_t peakhold_agc_buffer[BLOCK_SIZE];

sdr_signal_processor_t *mp_sdr_signal_processor = 0;
q15_t m_input_buffer[BLOCK_SIZE * 2];
q15_t m_output_buffer[BLOCK_SIZE * 2];
struct sdr_algorithm_table_t
{
	SDR_SIGNAL_PROCESSING_TYPE_t type;
	sdr_signal_processor_t *(*factory_func)(void);
};

struct sdr_algorithm_table_t algorithm_table[] = {
	{SDR_AM_RX, get_am_rx_module},
	{SDR_AM_TX, get_am_tx_module},
	{SDR_FM_RX, get_fm_rx_module},
	{SDR_FM_TX, get_fm_tx_process},
	{SDR_CW_TX, get_cw_tx_module},
	{SDR_CW_RX, get_ssb_rx_usb_module},
	{SDR_LSB_RX, get_ssb_rx_lsb_module},
	{SDR_LSB_TX, get_ssb_tx_lsb_module},
	{SDR_USB_RX, get_ssb_rx_usb_module},
	{SDR_USB_TX, get_ssb_tx_usb_module},
	{SDR_LAST_INVALID, 0}};

static sdr_signal_processor_t *get_processor(SDR_SIGNAL_PROCESSING_TYPE_t type)
{
	for (int i = 0; algorithm_table[i].type != SDR_LAST_INVALID; i++)
	{
		if (algorithm_table[i].type == type)
		{
			return algorithm_table[i].factory_func();
		}
	}
	return 0;
}

static void sdr_dispatch_input(uint16_t *data, size_t size)
{
	mp_sdr_signal_processor->on_input_captured(mp_sdr_signal_processor, data, (uint16_t)size);
}

void sdr_set_modulation_mode(SDR_SIGNAL_PROCESSING_TYPE_t mode)
{
	if (mp_sdr_signal_processor != 0)
	{
		audio_input_capture_stop(mp_sdr_signal_processor->input_ch);
		audio_output_stop(mp_sdr_signal_processor->output_ch);
		mp_sdr_signal_processor->deinit(mp_sdr_signal_processor);
	}
	else
	{
		arm_fir_init_q15(&baseband_lpf, BASEBAND_LPF_TAP_NUM, baseband_lpf_taps, baseband_lpf_state, BLOCK_SIZE);
		audio_input_capture_init(SAMPLING_RATE, (uint16_t *)m_input_buffer, BLOCK_SIZE * 2);
		audio_output_init(SDR_TX_IF_DAC_CH, SAMPLING_RATE, (uint16_t *)m_output_buffer, 2 * BLOCK_SIZE);
		audio_output_init(SDR_RX_SPK_DAC_CH, SAMPLING_RATE, (uint16_t *)m_output_buffer, 2 * BLOCK_SIZE);
	}
	sdr_signal_processor_t *p_processor = get_processor(mode);
	if (p_processor == 0)
	{
		return; //fail
	}
	mp_sdr_signal_processor = p_processor;
	p_processor->create(p_processor, BLOCK_SIZE);
	audio_input_capture_start(p_processor->input_ch, sdr_dispatch_input);
	audio_output_start(p_processor->output_ch, 0);
}
uint16_t sdr_get_rssi()
{
	if (mp_sdr_signal_processor)
	{
		return mp_sdr_signal_processor->rssi;
	}
	return 0;
}
void sdr_apply_baseband_lpf(q15_t *data, int size)
{
	arm_fir_q15(&baseband_lpf, data, data, size);
}

void freq_conv(q15_t *re, q15_t *im, q15_t *re_result,
			   q15_t *im_result, int blocksize, uint8_t up)
{
	q15_t tbl_cos[4] = {1, 0, -1, 0};
	if (im != NULL)
	{
		if (up)
		{
			for (int i = 0; i < blocksize; i++)
			{
				q15_t sin_t = tbl_cos[(i + 1) % 4];
				q15_t cos_t = tbl_cos[i % 4];
				q15_t re_t = re[i];
				q15_t im_t = im[i];
				re_result[i] = re_t * cos_t - im_t * sin_t;
				if (im_result != NULL)
					im_result[i] = re_t * sin_t + im_t * cos_t;
			}
		}
		else
		{
			for (int i = 0; i < blocksize; i++)
			{
				q15_t sin_t = tbl_cos[(i + 1) % 4];
				q15_t cos_t = tbl_cos[i % 4];
				q15_t re_t = re[i];
				q15_t im_t = im[i];
				re_result[i] = re_t * cos_t + im_t * sin_t;
				if (im_result != NULL)
					im_result[i] = -re_t * sin_t + im_t * cos_t;
			}
		}
	}
	else
	{
		if (up)
		{
			for (int i = 0; i < blocksize; i++)
			{
				q15_t sin_t = tbl_cos[(i + 1) % 4];
				q15_t cos_t = tbl_cos[i % 4];
				q15_t re_t = re[i];
				re_result[i] = re_t * cos_t;
				if (im_result != NULL)
					im_result[i] = re_t * sin_t;
			}
		}
		else
		{
			for (int i = 0; i < blocksize; i++)
			{
				q15_t sin_t = tbl_cos[(i + 1) % 4];
				q15_t cos_t = tbl_cos[i % 4];
				q15_t re_t = re[i];
				re_result[i] = re_t * cos_t;
				if (im_result != NULL)
					im_result[i] = -re_t * sin_t;
			}
		}
	}
}

q15_t m_dc_remove_integral = 0;
void dc_remove(q15_t *data, int blocksize)
{
	for (int i = 0; i < blocksize; i++)
	{
		data[i] -= m_dc_remove_integral;
		m_dc_remove_integral += (q15_t)((q31_t)data[i] * DC_REMOVE_COEFF_Q15 / 0x7fff);
	}
}
void add_offset(q15_t *data, int blocksize)
{
	for (int i = 0; i < blocksize; i++)
	{
		data[i] += OFFSET_Q15;
	}
}

q15_t m_agc_hold = 0;
#define AGC_SLOW_COEFF 700
#define AGC_FAST_COEFF 3510

uint16_t sdr_rssi_calc_peak_hold(q15_t *data, int blocksize)
{
	arm_abs_q15(data, peakhold_agc_buffer, blocksize);
	q15_t ave;
	arm_mean_q15(peakhold_agc_buffer, blocksize, &ave);
	m_agc_hold = ((q31_t)(0xffff - AGC_SLOW_COEFF) * m_agc_hold + AGC_SLOW_COEFF * ave) >> 16;
	return (uint16_t)m_agc_hold;
}
uint16_t sdr_rssi_calc_average(q15_t *data, int blocksize)
{
	q15_t ave;
	arm_mean_q15(data, blocksize, &ave);
	m_agc_hold = ((q31_t)(0xffff - AGC_FAST_COEFF) * m_agc_hold + AGC_FAST_COEFF * ave) >> 16;
	return (uint16_t)m_agc_hold;
}