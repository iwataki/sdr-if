/*
 * CWTransmit.cpp
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#include "tone_generate.h"
#include "cw_transmit.h"
#include <arm_math.h>
#include "util.h"
#include "signal_buffer_manage.h"

typedef struct
{
	q15_t tone_freq;
	int blocksize;
	tone_generator_state_t tone_cos;
	tone_generator_state_t tone_sin;
	q15_t *signal_i;
	q15_t *signal_q;
	sdr_signal_processor_t sdr_signal_processor;
} cw_tx_state_t;

static void cw_tx_init(sdr_signal_processor_t *inst, uint16_t blocksize)
{
	cw_tx_state_t *instance = container_of(inst, cw_tx_state_t, sdr_signal_processor);
	instance->signal_i = alloc_buffer(blocksize);
	instance->signal_q = alloc_buffer(blocksize);
	tone_generator_init(&instance->tone_cos,blocksize,SAMPLING_RATE);
	tone_generator_init(&instance->tone_sin,blocksize,SAMPLING_RATE);
	instance->blocksize = blocksize;
}

static void cw_tx_process(sdr_signal_processor_t *inst,uint16_t *input, uint16_t size)
{
	cw_tx_state_t *instance = container_of(inst, cw_tx_state_t, sdr_signal_processor);
	tone_generator_process(&instance->tone_cos,instance->tone_freq,instance->signal_i,0);
	tone_generator_process(&instance->tone_sin,instance->tone_freq,instance->signal_q, -0x2000);
	uint16_t*buf;
	uint16_t sz;
	audio_output_get_buffer(instance->sdr_signal_processor.output_ch,&buf,&sz);
	freq_conv(instance->signal_i,instance->signal_q,(q15_t*)buf,0,sz,1);
	add_offset((q15_t*)buf,sz);
}

static void cw_tx_deinit(sdr_signal_processor_t *inst)
{
	cw_tx_state_t *instance = container_of(inst, cw_tx_state_t, sdr_signal_processor);
	free_buffer(instance->signal_i);
	free_buffer(instance->signal_q);
}

cw_tx_state_t m_cw_tx_state;
sdr_signal_processor_t *get_cw_tx_module()
{
	m_cw_tx_state.sdr_signal_processor.on_input_captured = cw_tx_process;
	m_cw_tx_state.sdr_signal_processor.create = cw_tx_init;
	m_cw_tx_state.sdr_signal_processor.deinit = cw_tx_deinit;
	m_cw_tx_state.sdr_signal_processor.input_ch=SDR_TX_MIC_ADC_CH;
	m_cw_tx_state.sdr_signal_processor.output_ch=SDR_TX_IF_DAC_CH;
	return &m_cw_tx_state.sdr_signal_processor;
}
