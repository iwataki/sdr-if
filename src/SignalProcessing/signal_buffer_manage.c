#include "signal_buffer_manage.h"
#include <stdlib.h>

q15_t* alloc_buffer(uint16_t size){
    q15_t*allocated = malloc(sizeof(q15_t)*size);
    return allocated;
}
void free_buffer(q15_t*buff){
    free(buff);
}