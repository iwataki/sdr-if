/*
 * ToneGenerator.cpp
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#include "tone_generate.h"

void tone_generator_init(tone_generator_state_t*state,int block_size,int sampling_freq){
	state->block_size=block_size;
	state->fs=sampling_freq;
	state->phaseCos=0;
}
void tone_generator_process(tone_generator_state_t*state,int freq,q15_t*data,q15_t phase_offset){
	q15_t dp=(int)0x7fff*(int)freq/state->fs;
	for(int i=0;i<state->block_size;i++){
		state->phaseCos+=dp;
		data[i]=arm_cos_q15(state->phaseCos+phase_offset);
	}
}


