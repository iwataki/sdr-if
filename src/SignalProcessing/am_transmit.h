/*
 * AMTransmit.h
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#ifndef SRC_SIGNALPROCESSING_AMTRANSMIT_H_
#define SRC_SIGNALPROCESSING_AMTRANSMIT_H_
#include "sdr_common.h"

sdr_signal_processor_t*get_am_tx_module();
#endif /* SRC_SIGNALPROCESSING_AMTRANSMIT_H_ */
