/*
 * SSBReceive.h
 *
 *  Created on: 2019/06/30
 *      Author: �@��Y
 */

#ifndef SRC_SIGNALPROCESSING_SSBRECEIVE_H_
#define SRC_SIGNALPROCESSING_SSBRECEIVE_H_
#include "sdr_common.h"

sdr_signal_processor_t*get_ssb_rx_usb_module();
sdr_signal_processor_t*get_ssb_rx_lsb_module();

#endif /* SRC_SIGNALPROCESSING_SSBRECEIVE_H_ */
