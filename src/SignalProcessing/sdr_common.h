/*
 * DSPCommon.h
 *
 *  Created on: 2019/07/06
 *      Author: S.Iwataki
 */

#ifndef SRC_SIGNALPROCESSING_DSPCOMMON_H_
#define SRC_SIGNALPROCESSING_DSPCOMMON_H_

#include <arm_math.h>
#include <stdint.h>
#include "audio_input.h"
#include "audio_output.h"

#define SAMPLING_RATE 48000
#define BLOCK_SIZE 512

#define SDR_TX_MIC_ADC_CH 12
#define SDR_RX_IF_ADC_CH 11
#define SDR_TX_IF_DAC_CH 2
#define SDR_RX_SPK_DAC_CH 3

struct sdr_signal_processor_s;
typedef struct sdr_signal_processor_s sdr_signal_processor_t;
typedef void (*sdr_signal_process_create_t)(struct sdr_signal_processor_s*instance,uint16_t block_size);
typedef void (*sdr_signal_process_process_t)(struct sdr_signal_processor_s*instance,uint16_t*data,uint16_t block_size);
typedef void (*sdr_signal_process_deinit_t)(struct sdr_signal_processor_s*instance);
struct sdr_signal_processor_s
{
	sdr_signal_process_process_t on_input_captured;
	sdr_signal_process_create_t create;
	sdr_signal_process_deinit_t deinit;
	uint16_t rssi;
	uint8_t input_ch;
	uint8_t output_ch;
};

#define SDR_RX_INPUT_CH 0
#define SDR_RX_OUTPUT_CH 0
#define SDR_TX_INPUT_CH 1
#define SDR_TX_OUTPUT_CH 1
#define DC_REMOVE_COEFF_Q15 1 
#define OFFSET_Q15 0x7fff

typedef enum{
	SDR_AM_RX=0,
	SDR_AM_TX,
	SDR_LSB_RX,
	SDR_LSB_TX,
	SDR_USB_RX,
	SDR_USB_TX,
	SDR_CW_RX,
	SDR_CW_TX,
	SDR_FM_RX,
	SDR_FM_TX,
	SDR_LAST_INVALID
}SDR_SIGNAL_PROCESSING_TYPE_t;
void sdr_set_modulation_mode(SDR_SIGNAL_PROCESSING_TYPE_t mode);
uint16_t sdr_get_rssi();
#define BASEBAND_LPF_TAP_NUM 64
extern int16_t baseband_lpf_taps[BASEBAND_LPF_TAP_NUM];
void sdr_apply_baseband_lpf(q15_t*data,int size);
void freq_conv(q15_t *re, q15_t *im, q15_t *re_result, q15_t *im_result, int blocksize, uint8_t up);
void dc_remove(q15_t*data,int blocksize);
void add_offset(q15_t*data,int blocksize);
uint16_t sdr_rssi_calc_peak_hold(q15_t*data,int blocksize);
uint16_t sdr_rssi_calc_average(q15_t*data,int blocksize);

#endif /* SRC_SIGNALPROCESSING_DSPCOMMON_H_ */
