/*
 * AMReceive.cpp
 *
 *  Created on: 2019/07/07
 *      Author: �@��Y
 */

#include "am_receive.h"
#include "util.h"
#include "signal_buffer_manage.h"

#define AM_RX_DECIMATION_FACTOR 4

typedef struct
{
	sdr_signal_processor_t sdr_signal_processor;
	arm_fir_decimate_instance_q15 r_decimator;
	arm_fir_decimate_instance_q15 l_decimator;
	arm_fir_interpolate_instance_q15 result_interporator;
	q15_t *workbuff_1;
	q15_t *workbuff_2;
	int blocksize;
	q15_t integral;
} am_rx_state_t;

static void am_rx_init(sdr_signal_processor_t *instance, uint16_t blocksize)
{
	am_rx_state_t *inst = container_of(instance, am_rx_state_t, sdr_signal_processor);
	inst->workbuff_1 = alloc_buffer(blocksize);
	inst->workbuff_2 = alloc_buffer(blocksize);
	arm_fir_interpolate_init_q15(&inst->result_interporator, AM_RX_DECIMATION_FACTOR, BASEBAND_LPF_TAP_NUM, baseband_lpf_taps, alloc_buffer((blocksize + BASEBAND_LPF_TAP_NUM) / AM_RX_DECIMATION_FACTOR - 1), blocksize / AM_RX_DECIMATION_FACTOR);
	arm_fir_decimate_init_q15(&inst->r_decimator, BASEBAND_LPF_TAP_NUM, AM_RX_DECIMATION_FACTOR, baseband_lpf_taps, alloc_buffer(BASEBAND_LPF_TAP_NUM + blocksize - 1), blocksize);
	arm_fir_decimate_init_q15(&inst->l_decimator, BASEBAND_LPF_TAP_NUM, AM_RX_DECIMATION_FACTOR, baseband_lpf_taps, alloc_buffer(BASEBAND_LPF_TAP_NUM + blocksize - 1), blocksize);
}

static void am_rx_process(sdr_signal_processor_t *instance, uint16_t *input, uint16_t size)
{
	am_rx_state_t *inst = container_of(instance, am_rx_state_t, sdr_signal_processor);
	dc_remove((q15_t*)input,size);
	freq_conv((q15_t*)input, 0, inst->workbuff_1, inst->workbuff_2, size, 0);
	arm_fir_decimate_q15(&inst->r_decimator, inst->workbuff_1, inst->workbuff_1, size);
	arm_fir_decimate_q15(&inst->l_decimator, inst->workbuff_2, inst->workbuff_2, size);
	int decimated_size = size / AM_RX_DECIMATION_FACTOR;
	for (int i = 0; i < decimated_size; i++)
	{
		q15_t r_squred = ((int32_t)inst->workbuff_1[i] * inst->workbuff_1[i]) >> 16;
		q15_t l_squred = ((int32_t)inst->workbuff_2[i] * inst->workbuff_2[i]) >> 16;
		q15_t sig_norm;
		arm_sqrt_q15(r_squred / 2 + l_squred / 2, &sig_norm);
		inst->workbuff_1[i] = sig_norm;
	}
	instance->rssi=sdr_rssi_calc_average(inst->workbuff_1,decimated_size);
	//remove dc component
	for (int i = 0; i < decimated_size; i++)
	{
		inst->workbuff_1[i] -= inst->integral;
		inst->integral += inst->workbuff_1[i];
	}
	q15_t*output_buff;
	uint16_t sz;
	audio_output_get_buffer(instance->output_ch,(uint16_t**)&output_buff,&sz);
	arm_fir_interpolate_q15(&inst->result_interporator, inst->workbuff_1, output_buff, decimated_size);
	add_offset(output_buff,sz);
}

static void am_rx_destroy(sdr_signal_processor_t *instance)
{
	am_rx_state_t *inst = container_of(instance, am_rx_state_t, sdr_signal_processor);
	free_buffer(inst->workbuff_1);
	free_buffer(inst->workbuff_2);
	free_buffer(inst->l_decimator.pState);
	free_buffer(inst->r_decimator.pState);
	free_buffer(inst->result_interporator.pState);
}

am_rx_state_t m_am_rx_state;

sdr_signal_processor_t *get_am_rx_module()
{
	m_am_rx_state.sdr_signal_processor.on_input_captured = am_rx_process;
	m_am_rx_state.sdr_signal_processor.create = am_rx_init;
	m_am_rx_state.sdr_signal_processor.deinit = am_rx_destroy;
	m_am_rx_state.sdr_signal_processor.input_ch=SDR_RX_IF_ADC_CH;
	m_am_rx_state.sdr_signal_processor.output_ch=SDR_RX_SPK_DAC_CH;
	return &m_am_rx_state.sdr_signal_processor;
}
