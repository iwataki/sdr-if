/*
 * FMReceive.cpp
 *
 *  Created on: 2020/01/18
 *      Author: �@��Y
 */

#include "cordic.h"
#include <arm_math.h>
#include "fm_receive.h"
#include "util.h"
#include "signal_buffer_manage.h"

typedef struct
{
	arm_fir_instance_q15 audio_filter;
	q15_t *workbuff_1;
	q15_t *workbuff_2;
	int blocksize;
	q15_t differentiator_last_value;
	sdr_signal_processor_t sdr_signal_processor;

} fm_rx_state_t;

static void fm_rx_init(sdr_signal_processor_t *inst, uint16_t blocksize)
{
	fm_rx_state_t *instance = container_of(inst, fm_rx_state_t, sdr_signal_processor);
	instance->blocksize = blocksize;
	instance->workbuff_1 = alloc_buffer(blocksize);
	instance->workbuff_2 = alloc_buffer(blocksize);
	//arm_fir_init_q31(&bandwidth_limitation_filter_l,BPF_TAP_NUM,const_cast<q31_t*>(bpf_taps),new q31_t[BPF_TAP_NUM+blocksize-1],blocksize);
	//arm_fir_init_q31(&bandwidth_limitation_filter_r,BPF_TAP_NUM,const_cast<q31_t*>(bpf_taps),new q31_t[BPF_TAP_NUM+blocksize-1],blocksize);
	arm_fir_init_q15(&instance->audio_filter, BASEBAND_LPF_TAP_NUM, baseband_lpf_taps, alloc_buffer(BASEBAND_LPF_TAP_NUM + blocksize - 1), blocksize);
}

static void fm_rx_process(sdr_signal_processor_t *inst, uint16_t *input, uint16_t size)
{
	fm_rx_state_t *instance = container_of(inst, fm_rx_state_t, sdr_signal_processor);
	dc_remove((q15_t*)input,size);
	freq_conv((q15_t*)input, 0, instance->workbuff_1, instance->workbuff_2, instance->blocksize, 0);

	//bandwidth limitting
	//arm_fir_q31(&bandwidth_limitation_filter_l,workbuff_1,workbuff_1,blocksize);
	//arm_fir_q31(&bandwidth_limitation_filter_r,workbuff_2,workbuff_2,blocksize);
	//convert to phase
	inst->rssi=sdr_rssi_calc_average((q15_t*)input,size);
	for (int i = 0; i < instance->blocksize; i++)
	{
		instance->workbuff_1[i] = cordic_atan2(instance->workbuff_1[i], instance->workbuff_2[i]);
	}
	//differentiate
	instance->workbuff_2[0] = instance->workbuff_1[0] - instance->differentiator_last_value;
	for (int i = 1; i < instance->blocksize; i++)
	{
		instance->workbuff_2[i] = instance->workbuff_1[i] - instance->workbuff_1[i - 1];
	}
	instance->differentiator_last_value = instance->workbuff_1[instance->blocksize - 1];

	q15_t *output_buff;
	uint16_t sz;
	audio_output_get_buffer(instance->sdr_signal_processor.output_ch, (uint16_t**)&output_buff, &sz);
	//lpf audio
	arm_fir_q15(&instance->audio_filter, instance->workbuff_2, output_buff, sz);
	add_offset(output_buff,sz);
}

static void fm_rx_deinit(sdr_signal_processor_t *inst){
	fm_rx_state_t *instance = container_of(inst, fm_rx_state_t, sdr_signal_processor);
	free_buffer(instance->workbuff_1);
	free_buffer(instance->workbuff_2);
	free_buffer(instance->audio_filter.pState);
}

fm_rx_state_t m_fm_rx_state;

sdr_signal_processor_t *get_fm_rx_module(void)
{
	m_fm_rx_state.sdr_signal_processor.on_input_captured = fm_rx_process;
	m_fm_rx_state.sdr_signal_processor.create = fm_rx_init;
	m_fm_rx_state.sdr_signal_processor.deinit = fm_rx_deinit;
	m_fm_rx_state.sdr_signal_processor.input_ch=SDR_RX_INPUT_CH;
	m_fm_rx_state.sdr_signal_processor.output_ch=SDR_RX_SPK_DAC_CH;
	return &m_fm_rx_state.sdr_signal_processor;
}