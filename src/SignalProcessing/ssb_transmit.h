/*
 * SSBTransmit.h
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#ifndef SRC_SIGNALPROCESSING_SSBTRANSMIT_H_
#define SRC_SIGNALPROCESSING_SSBTRANSMIT_H_
#include "sdr_common.h"
sdr_signal_processor_t*get_ssb_tx_usb_module();
sdr_signal_processor_t*get_ssb_tx_lsb_module();


#endif /* SRC_SIGNALPROCESSING_SSBTRANSMIT_H_ */
