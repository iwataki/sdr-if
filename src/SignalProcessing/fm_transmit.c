/*
 * FMTransmit.cpp
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#include "cordic.h"
#include <arm_math.h>
#include "fm_transmit.h"
#include "util.h"
#include "signal_buffer_manage.h"

typedef struct
{
	arm_fir_instance_q15 baseband_lpf;
	q15_t *workbuff_i;
	q15_t *workbuff_r;
	int blocksize;

	q15_t integrator_last_value;
	sdr_signal_processor_t sdr_signal_processor;
} fm_tx_state_t;

static void fm_tx_init(sdr_signal_processor_t *inst, uint16_t blocksize)
{
	fm_tx_state_t *instance = container_of(inst, fm_tx_state_t, sdr_signal_processor);
	instance->blocksize = blocksize;
	instance->workbuff_i = alloc_buffer(blocksize);
	instance->workbuff_r = alloc_buffer(blocksize);
	arm_fir_init_q15(&instance->baseband_lpf, BASEBAND_LPF_TAP_NUM,
					 baseband_lpf_taps,
					 alloc_buffer(BASEBAND_LPF_TAP_NUM + blocksize - 1), blocksize);
}

static void fm_tx_process(sdr_signal_processor_t *inst, uint16_t *input, uint16_t size)
{
	fm_tx_state_t *instance = container_of(inst, fm_tx_state_t, sdr_signal_processor);
	//baseband bandwidth limit
	dc_remove((q15_t*)input,size);
	arm_fir_q15(&instance->baseband_lpf, (q15_t*)input, instance->workbuff_r, instance->blocksize);

	instance->workbuff_r[0] += instance->integrator_last_value;
	for (int i = 1; i < instance->blocksize; i++)
	{ //integrate
		instance->workbuff_r[i] += instance->workbuff_r[i - 1];
	}
	instance->integrator_last_value = instance->workbuff_r[instance->blocksize - 1];

	for (int i = 0; i < instance->blocksize; i++)
	{
		int phase = instance->workbuff_r[i];
		instance->workbuff_r[i] = cordic_cos(phase);
		instance->workbuff_i[i] = cordic_sin(phase);
	}
	q15_t *output_buff;
	uint16_t sz;
	audio_output_get_buffer(instance->sdr_signal_processor.output_ch, (uint16_t**)&output_buff, &sz);
	freq_conv(instance->workbuff_r, instance->workbuff_i, output_buff, 0, instance->blocksize, 1);
	add_offset(output_buff,sz);
}


static void fm_tx_deinit(sdr_signal_processor_t *inst){
	fm_tx_state_t *instance = container_of(inst, fm_tx_state_t, sdr_signal_processor);
	free_buffer(instance->workbuff_r);
	free_buffer(instance->workbuff_i);
	free_buffer(instance->baseband_lpf.pState);
}

fm_tx_state_t m_fm_tx_state;
sdr_signal_processor_t*get_fm_tx_process(){
	m_fm_tx_state.sdr_signal_processor.on_input_captured = fm_tx_process;
	m_fm_tx_state.sdr_signal_processor.create = fm_tx_init;
	m_fm_tx_state.sdr_signal_processor.deinit = fm_tx_deinit;
	m_fm_tx_state.sdr_signal_processor.input_ch=SDR_TX_MIC_ADC_CH;
	m_fm_tx_state.sdr_signal_processor.output_ch=SDR_TX_IF_DAC_CH;
	return &m_fm_tx_state.sdr_signal_processor;
}
