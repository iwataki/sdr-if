/*
 * AMTransmit.cpp
 *
 *  Created on: 2019/07/06
 *      Author: �@��Y
 */

#include <arm_math.h>
#include "am_transmit.h"
#include "util.h"
#include "signal_buffer_manage.h"

typedef struct
{
	sdr_signal_processor_t sdr_signal_processor;
	arm_fir_instance_q15 baseband_lpf;
	int blocksize;
} am_transmit_state_t;

static void am_tx_init(sdr_signal_processor_t *inst, uint16_t blocksize)
{
	am_transmit_state_t *instance = container_of(inst, am_transmit_state_t, sdr_signal_processor);
	instance->blocksize = blocksize;
	arm_fir_init_q15(&instance->baseband_lpf, BASEBAND_LPF_TAP_NUM, baseband_lpf_taps, alloc_buffer(blocksize + BASEBAND_LPF_TAP_NUM - 1), blocksize);
}

static void am_tx_process(sdr_signal_processor_t *inst, uint16_t *input, uint16_t size)
{
	am_transmit_state_t *instance = container_of(inst, am_transmit_state_t, sdr_signal_processor);
	dc_remove((q15_t *)input, size);
	//baseband bandwidth limit
	arm_fir_q15(&instance->baseband_lpf, (q15_t *)input, (q15_t *)input, instance->blocksize);
	for(int i=0;i<size;i++){
		input[i]=input[i]/4+0x7fff/4;//limit amplitude and add dc component
	}
	//convert 12kHz
	q15_t *output_buff;
	uint16_t sz;
	audio_output_get_buffer(instance->sdr_signal_processor.output_ch, (uint16_t **)&output_buff, &sz);
	freq_conv((q15_t *)input, 0, output_buff, 0, instance->blocksize, 1);
	//add dc offset
	add_offset(output_buff, sz);
}

static void am_tx_deinit(sdr_signal_processor_t *inst)
{
	am_transmit_state_t *instance = container_of(inst, am_transmit_state_t, sdr_signal_processor);
	free_buffer(instance->baseband_lpf.pState);
}

static am_transmit_state_t m_am_trannsmit_state;
sdr_signal_processor_t *get_am_tx_module()
{
	m_am_trannsmit_state.sdr_signal_processor.on_input_captured = am_tx_process;
	m_am_trannsmit_state.sdr_signal_processor.create = am_tx_init;
	m_am_trannsmit_state.sdr_signal_processor.deinit = am_tx_deinit;
	m_am_trannsmit_state.sdr_signal_processor.input_ch = SDR_TX_MIC_ADC_CH;
	m_am_trannsmit_state.sdr_signal_processor.output_ch = SDR_TX_IF_DAC_CH;
	return &m_am_trannsmit_state.sdr_signal_processor;
}