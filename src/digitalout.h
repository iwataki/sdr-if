/*
 * digitalout.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef DIGITALOUT_H_
#define DIGITALOUT_H_
#include <inttypes.h>
#include "stm32f30x_conf.h"

typedef struct {
	GPIO_TypeDef*gpio;
	uint16_t pin;
}DIGITALOUT_t;

void init_digitalout(DIGITALOUT_t*d);
void digitalout_set(DIGITALOUT_t*d);
void digitalout_reset(DIGITALOUT_t*d);

#endif /* DIGITALOUT_H_ */
