/*
 * adc.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef ADC_H_
#define ADC_H_

#include <inttypes.h>
#include "stm32f30x_conf.h"

//reference voltage[mV]
#define ADC_VREF 3300
typedef struct{
	ADC_TypeDef*adc;
	GPIO_TypeDef*gpio;
	uint32_t gpio_pin;
	uint32_t adc_channel;
}ADC_t;

void adc_init(ADC_t*adc,ADC_TypeDef*adc_instance, GPIO_TypeDef*gpio,uint32_t gpio_pin,uint32_t adc_channel);
uint16_t adc_get(ADC_t*adc);

#endif /* ADC_H_ */
