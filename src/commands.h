#pragma once
#include "data_structure.h"
#include <stddef.h>

typedef enum{
    SET_BFO_FREQ,
    SET_XTAL_FREQ,
    SET_TRX_CIRCIUT_POWER,
    SET_MODULATION_MODE,
    SET_AUX_VFO_FREQ,
    ENABLE_AUX_VFO,
    DISABLE_AUX_VFO,
    SET_SQULCH_LEVEL,
    COMMANDS_END
}command_ids_t;

void commands_init(SYSTEM_STATUS_t*status);
int32_t commands_dispatch(command_ids_t opcode,uint8_t*params, size_t param_length);