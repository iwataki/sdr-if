#include "gpio.h"

void gpio_enable(GPIO_TypeDef*gpio){
    switch ((uint32_t)gpio)
	{
	case GPIOA_BASE:
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
		break;
	case GPIOB_BASE:
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB, ENABLE);
		break;
	case GPIOC_BASE:
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOC,ENABLE);
		break;
	case GPIOD_BASE:
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOD,ENABLE);
		break;
	case GPIOE_BASE:
		RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOE,ENABLE);
		break;
	default:
		break;
	}
}