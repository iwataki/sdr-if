#pragma once
#include <stdint.h>

//バッファが空くと呼ばれる
typedef void(*write_callback_t)();

void audio_output_init(uint8_t chan,uint32_t samplingrate,uint16_t*buffer,uint16_t size);
//return 0 success
void audio_output_get_buffer(uint8_t chan,uint16_t**pp_buffer,uint16_t*p_size);
int8_t audio_output_start(uint8_t ch,write_callback_t cb);
int8_t audio_output_stop(uint8_t ch);
void audio_output_enable(uint8_t ch);
void audio_output_disable(uint8_t ch);