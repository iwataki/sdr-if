#include "commands.h"
#include <errno.h>
#include "stdint_serialize.h"

static SYSTEM_STATUS_t *state;

typedef struct
{
    command_ids_t id;
    int32_t (*func)(uint8_t *params, size_t param_len);
} command_entry_t;

static int32_t cmd_set_bfo_freq(uint8_t *params, size_t param_length)
{
    if (param_length != sizeof(uint32_t))
    {
        return -EILSEQ;
    }
    state->calibration.bfo_freq = uint32_decode(params);
    return 0;
}
static int32_t cmd_set_xtal_freq(uint8_t *params, size_t param_length)
{
    if (param_length != sizeof(uint32_t))
    {
        return -EILSEQ;
    }
    state->calibration.crystal = uint32_decode(params);
    return 0;
}
static int32_t cmd_set_trx_circuit_power(uint8_t *params, size_t param_length)
{
    if (param_length != sizeof(uint8_t))
    {
        return -EILSEQ;
    }
    uint8_t req = uint8_decode(params);
    int32_t retcode = 0;
    switch (req)
    {
    case 0:
        state->radio_circuit_status.is_transmitter_enable = 0;
        state->radio_circuit_status.is_receiver_enable = 0;
        break;
    case 1:
        state->radio_circuit_status.is_transmitter_enable = 0;
        state->radio_circuit_status.is_receiver_enable = 1;
        break;
    case 2:
        state->radio_circuit_status.is_transmitter_enable = 1;
        state->radio_circuit_status.is_receiver_enable = 0;
        break;
    default:
        retcode = -EINVAL;
        break;
    }
    return retcode;
}
static int32_t cmd_set_modulation_mode(uint8_t *params, size_t param_length)
{
    if (param_length != sizeof(uint8_t))
    {
        return -EILSEQ;
    }
    uint8_t req = uint8_decode(params);
    if (req >= MOD_INVALID)
    {
        return -EINVAL;
    }
    state->modulation_mode = req;
    return 0;
}
static int32_t cmd_set_aux_vfo_freq(uint8_t *params, size_t param_length)
{
    if (param_length != sizeof(uint32_t))
    {
        return -EILSEQ;
    }
    state->aux_vfo_status.freqency = uint32_decode(params);
    return 0;
}
static int32_t cmd_enable_aux_vfo(uint8_t *params, size_t param_length)
{
    if (param_length !=0)
    {
        return -EILSEQ;
    }
    state->aux_vfo_status.enable=1;
    return 0;

}
static int32_t cmd_disable_aux_vfo(uint8_t *params, size_t param_length)
{
    if (param_length !=0)
    {
        return -EILSEQ;
    }
    state->aux_vfo_status.enable=0;
    return 0;
}
static int32_t cmd_set_squlch_level(uint8_t *params, size_t param_length)
{
    if(param_length!=sizeof(uint16_t)){
        return -EILSEQ;
    }

    state->squelch_level=uint16_decode(params);
    return 0;
}

command_entry_t commands_table[] = {
    {SET_BFO_FREQ, cmd_set_bfo_freq},
    {SET_XTAL_FREQ, cmd_set_xtal_freq},
    {SET_TRX_CIRCIUT_POWER, cmd_set_trx_circuit_power},
    {SET_MODULATION_MODE, cmd_set_modulation_mode},
    {SET_AUX_VFO_FREQ, cmd_set_aux_vfo_freq},
    {ENABLE_AUX_VFO, cmd_enable_aux_vfo},
    {DISABLE_AUX_VFO, cmd_disable_aux_vfo},
    {SET_SQULCH_LEVEL, cmd_set_squlch_level},
    {COMMANDS_END, 0}};

void commands_init(SYSTEM_STATUS_t *status)
{
    state = status;
}
int32_t commands_dispatch(command_ids_t opcode, uint8_t *params, size_t param_length)
{
    state->operation_default_mode=0;
    for (int i = 0; commands_table[i].id < COMMANDS_END; i++)
    {
        if (commands_table[i].id == opcode)
        {
            return commands_table[i].func(params, param_length);
        }
    }
    return -EOPNOTSUPP;
}