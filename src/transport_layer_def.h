#pragma once
#include <stddef.h>
#include <stdint.h>

typedef enum{
    IDLE,
    PKT_RECEVING,
    PKT_AVAILABLE

}TRANSPORT_LAYER_STATE_t;

typedef enum{
    COMMAND_PKT,
    RESPONSE_PKT,
    EVENT_PKT
}TRANSPORT_PKT_KIND_t;

typedef struct{
    uint8_t(*get_rx_pkt)(uint8_t**data,size_t*size);
    uint8_t(*send_tx_pkt)(uint8_t*data,size_t size);
    TRANSPORT_LAYER_STATE_t(*poll)(void);
}transport_layer_t;