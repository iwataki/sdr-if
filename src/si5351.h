/*
 * si5351.h
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#ifndef SI5351_H_
#define SI5351_H_
#include "inttypes.h"
#define SI5351_ADDR 0xc0
#define PLL_MAX_FREQ 900000000
#define PLL_MIN_FREQ 600000000
#define MULTISYNTH_MAX 1800
#define MULTISYNTH_MIN 6
#define PLL_MULTISYNTH_MAX 90
#define PLL_MULTISYNTH_MIN 15

#define SI5351_OUTPUT_POWER_ON 0x00
#define SI5351_OUTPUT_POWER_OFF 0x80

typedef struct{
	uint32_t xtal_freq;

}SI5351_CONFIG_t;

void init_si5351(void);
void set_frequency(SI5351_CONFIG_t*d,uint8_t ch,uint32_t frequency);
void enable_clk(uint8_t ch);
void disable_clk(uint8_t ch);

#endif /* SI5351_H_ */
