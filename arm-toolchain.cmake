
include(CMakeForceCompiler)
set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_PROCESSOR arm)
SET(CMAKE_CROSSCOMPILING TRUE)

set(CROSS_COMPILE "arm-none-eabi-")

SET(CMAKE_C_COMPILER ${CROSS_COMPILE}gcc)
SET(CMAKE_CXX_COMPILER ${CROSS_COMPILE}g++)
#SET(CMAKE_C_LINK_EXECUTABLE ${CROSS_COMPILE}g++)
#SET(CMAKE_CXX_LINK_EXECUTABLE ${CROSS_COMPILE}g++)
set(CMAKE_AR ${CROSS_COMPILE}ar CACHE PATH "" FORCE)

CMAKE_FORCE_C_COMPILER(${CMAKE_C_COMPILER} GCC)
CMAKE_FORCE_CXX_COMPILER(${CMAKE_CXX_COMPILER} GCC)


set(ARM_COMPILE_OPTION "-march=armv7e-m -mcpu=cortex-m4 -mthumb -mfloat-abi=softfp -mfpu=fpv4-sp-d16 -O2 -g -Wall -Werror")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${ARM_COMPILE_OPTION} -std=gnu11")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${ARM_COMPILE_OPTION} -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics")


###linker config
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${ARM_COMPILE_OPTION} -nostartfiles -Xlinker --gc-sections -Xlinker --no-print-gc-sections --specs=nano.specs")



# search for programs in the build host directories
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
