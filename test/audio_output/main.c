#include "audio_input.h"
#include "audio_output.h"
#include "power_management.h"
#include "usart.h"
#include "stdio.h"
#include "SignalProcessing/sdr_common.h"
#include "stm32f30x_conf.h"

uint16_t buffer_in[256];
uint16_t buffer_out[512];
q15_t phase=0;
q15_t freq=1000;
void playback_cb(){
    uint16_t*buf;
    uint16_t sz;
    audio_output_get_buffer(SDR_RX_SPK_DAC_CH,&buf,&sz);
    for(int i=0;i<sz;i++){
        buf[i]=arm_sin_q15(phase)>>4;
        phase+=freq;
    }
}

int __io_putchar(int ch){
    usart_putc(1,(char)ch);
    return 0;
}

int main(void){
    init_usart(1,19200);
    DBGMCU->APB1FZ|=DBGMCU_APB1_FZ_DBG_TIM6_STOP;
    printf("audio output test\n");
    audio_output_init(SDR_RX_SPK_DAC_CH,48000,buffer_out,512);
    audio_output_start(SDR_RX_SPK_DAC_CH,playback_cb);
    while(1){
        system_idle();
    }
}