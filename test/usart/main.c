#include "usart.h"
#include "power_management.h"
#include "timer.h"
#include "stdio.h"

uint16_t count=1;
void timer_handler(void){
    count++;
}
int __io_putchar(int ch){
    usart_putc(1,(char)ch);
    return 0;
}

int main(void){
    init_usart(1,19200);
    timer_init(10,timer_handler);
    uint16_t lpcnt=0;
    while(1){
        system_idle();
        if(usart_is_received(1)){
            usart_putc(1,usart_getc_noblock(1));
        }
        if(count>100){
            count=0;
            printf("cnt=%d\n",lpcnt++);
        }
    }
}