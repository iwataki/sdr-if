#include "audio_input.h"
#include "audio_output.h"
#include "power_management.h"
#include "usart.h"
#include "stdio.h"
#include "SignalProcessing/sdr_common.h"
#include "stm32f30x_conf.h"

uint16_t buffer_in[256];
uint16_t buffer_out[256];
void capture_cb(uint16_t*data,size_t size){
    uint16_t*buf;
    uint16_t sz;
    audio_output_get_buffer(SDR_RX_SPK_DAC_CH,&buf,&sz);
    for(int i=0;i<sz;i++){
        buf[i]=data[i];
    }
}

int __io_putchar(int ch){
    usart_putc(1,(char)ch);
    return 0;
}

int main(void){
    init_usart(1,19200);
    DBGMCU->APB1FZ|=DBGMCU_APB1_FZ_DBG_TIM6_STOP;
    DBGMCU->APB2FZ|=DBGMCU_APB2_FZ_DBG_TIM1_STOP;
    printf("audio loopback test\n");
    audio_input_capture_init(48000,buffer_in,256);
    audio_output_init(SDR_RX_SPK_DAC_CH,48000,buffer_out,256);
    audio_input_capture_start(SDR_TX_MIC_ADC_CH,capture_cb);
    audio_output_start(SDR_RX_SPK_DAC_CH,0);
    while(1){
        system_idle();
    }
}