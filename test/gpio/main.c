#include "user_switch.h"
#include "digitalout.h"
#include "power_management.h"
#include "timer.h"
#include "hardware.h"


void loop(void){
    update_switch(&tx_enable_in);
    update_switch(&rx_enable_in);
    if(get_switch(&tx_enable_in)){
        digitalout_set(&transmitter_enable_out);
    }else{
        digitalout_reset(&transmitter_enable_out);
    }
    if(get_switch(&rx_enable_in)){
        digitalout_set(&receiver_enable_out);
    }else{
        digitalout_reset(&receiver_enable_out);
    }
}
int main(void){
    init_usersw(&tx_enable_in);
    init_usersw(&rx_enable_in);
    init_digitalout(&transmitter_enable_out);
    init_digitalout(&receiver_enable_out);
    timer_init(10,loop);
    while(1){
        system_idle();
    }
}