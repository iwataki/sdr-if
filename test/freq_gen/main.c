#include "si5351.h"
#include "power_management.h"


int main(void){
    init_si5351();
    SI5351_CONFIG_t cfg={.xtal_freq=25000000};
    set_frequency(&cfg,1,7100000);
    enable_clk(1);
    while(1){
        
        system_idle();//sleepするとswd接続できない
    }
}